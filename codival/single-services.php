<?php get_header(); ?>
<div class="page-container" data-slug="all">
	<section class="page-content">
    <div class="content-container">
			<div class="breadcrumb">
				<?php get_breadcrumb(); ?>
			</div>
		</div>
		<header class="page-content--header content-container">
			<h1 class="title-page"><?php the_title(); ?></h1>
		</header>
		<?php include(locate_template('templates/modules-libres.php')); ?>
		<?php include(locate_template('modules/module-similar.php')); ?>
	</section>
</div>
<?php get_footer(); ?>
