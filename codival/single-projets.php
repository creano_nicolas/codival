<?php get_header();?>


<div class="page-container" data-slug="project-detail">
	<section class="page-content ">
		<div class="content-container content-container__sm">
			<div class="breadcrumb">
				<?php get_breadcrumb(); ?>
			</div>
		</div>

		<!-- Titre de la publication -->
		<header class="page-content--header content-container content-container__sm">
			<h1 class="title-page"><?php the_title(); ?></h1>
		</header>

		<?php
				$module = array(
					"photos" => carbon_get_the_post_meta('projet_header_photos'),
				);
				if (count($module["photos"]) > 0){
					include(locate_template('modules/module-images.php'));
				}
		?>

		<?php include(locate_template('modules/module-share.php')); ?>

		<!-- Module Deux Bloc -->
		<?php
			$module = array(
				"text_title" => carbon_get_post_meta(get_the_ID(), "projet_text_title"),
				"text_1" => carbon_get_post_meta( get_the_ID(), "projet_text_1"),
				"text_2" => carbon_get_post_meta( get_the_ID(), "projet_text_2"),
				"text_cta" => carbon_get_post_meta( get_the_ID(), "projet_text_cta"),
				"link_cta" => carbon_get_post_meta( get_the_ID(), "projet_link_cta")
			);
			include(locate_template('modules/module-twoblocks.php'));
		?>

	<?php include(locate_template('templates/modules-libres.php')); ?>
	<?php include(locate_template('modules/module-similar.php')); ?>
	</section>
</div>

<?php get_footer(); ?>
