<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Carbon\Carbon;

function HTML($str){
	return htmlentities($str, ENT_QUOTES);
}

/*------------------------------------*\
	HOME
\*------------------------------------*/
Container::make( 'post_meta', __( '1/ Header carousel' ) )
	->show_on_post_type('page')
	->show_on_template('home.php')
	->add_fields(array(
		Field::make('complex', 'home_behead_slides', 'Slide')
			->set_collapsed(true)
			->setup_labels(array('plural_name' => 'Slides', 'singular_name' => 'Slide'))
			->add_fields(array(
				Field::make('textarea', 'title', 'Titre de la slide'),
				Field::make('image', 'image', 'Image')
					->set_width(50),
				Field::make('file', 'video', 'Vidéo')
					->set_width(50),
				Field::make('text', 'link_text', 'Texte du lien')
					->set_width(50),
				Field::make('text', 'link', 'Url du lien')
					->set_width(50),
			))
			->set_header_template( '<%- title %>' )
	));

	Container::make( 'post_meta', __( '2/ Codival' ) )
	->show_on_post_type('page')
	->show_on_template('home.php')
	->add_fields(array(
			Field::make('text', 'home_codibest_title_tl', 'Titre sur le côté'),
			Field::make('text', 'home_codibest_title', 'Titre du bloc')
				->set_width(50),
			Field::make('textarea', 'home_codibest_desc', 'Texte du bloc')
				->set_width(50),
			Field::make('text', 'home_codibest_link_text', 'Texte du lien')
				->set_width(50),
			Field::make('text', 'home_codibest_link', 'Url du lien')
				->set_width(50),
	));

	Container::make( 'post_meta', __( '3/ Nos activités' ) )
	->show_on_post_type('page')
	->show_on_template('home.php')
	->add_fields(array(
		Field::make('text', 'home_codiacti_title_tl', 'Titre sur le côté'),
		Field::make('text', 'home_codiacti_title', 'Titre du bloc')
			->set_width(50),
		Field::make('textarea', 'home_codiacti_desc', 'Texte du bloc')
			->set_width(50),

		Field::make('complex', 'home_codiacti_slides', 'Slide')
			->set_collapsed(true)
			->setup_labels(array('plural_name' => 'Slides', 'singular_name' => 'Slide'))
			->add_fields(array(
				Field::make('text', 'title', 'Titre de la slide')
					->set_width(50),
				Field::make('image', 'image', 'Image')
					->set_width(50),
				Field::make('text', 'link_text', 'Texte du lien')
					->set_width(50),
				Field::make('text', 'link', 'Url du lien')
					->set_width(50),
				Field::make('textarea', 'desc', 'Description')
					->set_width(100),
			))
		->set_header_template( '<%- title %>' )
	));

	Container::make( 'post_meta', __( '4/ Réseau' ) )
	->show_on_post_type('page')
	->show_on_template('home.php')
	->add_fields(array(
		Field::make('text', 'home_codires_title_tl', 'Titre sur le côté'),
		Field::make('text', 'home_codires_title', 'Titre du bloc')
			->set_width(50),
		Field::make('textarea', 'home_codires_desc', 'Texte du bloc')
			->set_width(50),

		Field::make('complex', 'home_codires_slides', 'Slide')
			->set_collapsed(true)
			->setup_labels(array('plural_name' => 'Slides', 'singular_name' => 'Slide'))
			->add_fields(array(
				Field::make('image', 'image', 'Logo')
					->set_width(25),
				Field::make('text', 'title', 'Titre de la slide')
					->set_width(25),
				Field::make('textarea', 'desc1', 'Texte noir')
					->set_width(25),
				Field::make('textarea', 'desc2', 'Texte orange')
					->set_width(25),
			))
			->set_header_template( '<%- title %>' )
	));



/*------------------------------------*\
	ACTUALITES
\*------------------------------------*/
Container::make( 'post_meta', __( '1/ Header' ) )
	->where("post_type", "=", 'post')
	->add_fields( array(
		Field::make('media_gallery', 'header_photos', 'Photos')
	));

/*------------------------------------*\
	MODULE LIBRE
\*------------------------------------*/

Container::make('post_meta', __('Contenu libre'))
	->where('post_template', 'IN', array('default'))
	->add_fields(array(

		Field::make('complex', 'type_de_module', 'Modules')
			->set_collapsed(true)
			->setup_labels(array('plural_name' => 'Modules', 'singular_name' => 'Modules'))

			// Module : Wysiwyg
			->add_fields('markup', 'Module WYSIWYG', array(
				Field::make( 'radio', 'color', 'Couleur de fond' )
				->add_options( array(
						'white' => 'Blanc',
						'green' => 'Vert'
				)),
				Field::make('rich_text', 'text', 'Contenus')
			))

			// Module : caroussel/image
			->add_fields('images', 'Module Carrousel/Image', array(
				Field::make('media_gallery', 'photos', 'Photos')
			))
			->add_fields('expertises',"Module multiblocs avec pictos", array(
				Field::make('complex', 'blocks', 'Blocs')
					->set_collapsed(true)
					->setup_labels(array('plural_name' => 'Blocs', 'singular_name' => 'Blocs'))
					->add_fields(array(
						Field::make('image', 'picto', 'type de picto' ),
						Field::make('text', 'title', 'Titre du  bloc')
							->set_width(50),
						Field::make('textarea', 'text', 'Texte du  bloc')
							->set_width(50),
					))
					->set_header_template( '<%- title %>' )
			))
					//Module : Codival - bhead
					// ->add_fields('bhead', 'Module Header slider', array(
					// 	Field::make('complex', 'slides', 'Slide')
					// 		->set_collapsed(true)
					// 		->setup_labels(array('plural_name' => 'Slides', 'singular_name' => 'Slide'))
					// 		->add_fields(array(
					// 			Field::make('textarea', 'title', 'Titre de la slide')
					// 				->set_width(40),
					// 			Field::make('image', 'image', 'Image')
					// 				->set_width(20),
					// 			Field::make('file', 'video', 'Vidéo')
					// 				->set_width(20),
					// 			Field::make('text', 'link', 'Url du lien')
					// 				->set_width(20),
					// 		))
					// 		->set_header_template( '<%- title %>' )
					// ))

					//Module : Codival - codibest
					// ->add_fields('codibest', 'Module Codibest', array(
					// 	Field::make('text', 'title_tl', 'Titre sur le côté'),
					// 	Field::make('text', 'title', 'Titre du bloc')
					// 		->set_width(50),
					// 	Field::make('textarea', 'desc', 'Texte du bloc')
					// 		->set_width(50),
					// 	Field::make('text', 'link', 'Url du lien')
					// 		->set_width(100),
					// ))

					//Module : Codival - acti
					// ->add_fields('acti', 'Module Activités', array(
					// 	Field::make('text', 'title_tl', 'Titre sur le côté'),
					// 	Field::make('text', 'title', 'Titre du bloc')
					// 		->set_width(50),
					// 	Field::make('textarea', 'desc', 'Texte du bloc')
					// 		->set_width(50),

					// 	Field::make('complex', 'slides', 'Slide')
					// 		->set_collapsed(true)
					// 		->setup_labels(array('plural_name' => 'Slides', 'singular_name' => 'Slide'))
					// 		->add_fields(array(
					// 			Field::make('text', 'title', 'Titre de la slide')
					// 				->set_width(50),
					// 			Field::make('image', 'image', 'Image')
					// 				->set_width(25),
					// 			Field::make('text', 'link', 'Url du lien')
					// 				->set_width(25),
					// 			Field::make('textarea', 'desc', 'Description')
					// 				->set_width(100),
					// 		))
					// 		->set_header_template( '<%- title %>' )
					// 	))

					//Module : Codival - codires
					// ->add_fields('codires', 'Module Codires', array(
					// 	Field::make('text', 'title_tl', 'Titre sur le côté'),
					// 	Field::make('text', 'title', 'Titre du bloc')
					// 		->set_width(50),
					// 	Field::make('textarea', 'desc', 'Texte du bloc')
					// 		->set_width(50),

					// 	Field::make('complex', 'slides', 'Slide')
					// 		->set_collapsed(true)
					// 		->setup_labels(array('plural_name' => 'Slides', 'singular_name' => 'Slide'))
					// 		->add_fields(array(
					// 			Field::make('image', 'image', 'Logo')
					// 				->set_width(25),
					// 			Field::make('text', 'title', 'Titre de la slide')
					// 				->set_width(25),
					// 			Field::make('textarea', 'desc1', 'Texte noir')
					// 				->set_width(25),
					// 			Field::make('textarea', 'desc2', 'Texte orange')
					// 				->set_width(25),
					// 		))
					// 		->set_header_template( '<%- title %>' )
					// ))

					//Module : LogoTabs
					->add_fields('logotabs', 'Module Logos avec onglets', array(
						Field::make('text', 'type', 'Type de logo'),
						Field::make('complex', 'tabs', 'Onglets')
							->set_collapsed(true)
							->setup_labels(array('plural_name' => 'Onglets', 'singular_name' => 'Onglet'))
							->add_fields(array(
								Field::make('text', 'tabname', "Nom de l'onglet"),
								Field::make('complex', 'logos', 'Logos')
								->set_collapsed(true)
								->setup_labels(array('plural_name' => 'Logos', 'singular_name' => 'Logo'))
								->add_fields(array(
									Field::make('image', 'img', "Logo")
										->set_width(50),
									Field::make('text', 'url', "Lien externe")
										->set_width(50),
								))
								->set_header_template( '<%- url %>' ),
								Field::make('rich_text', 'text', 'Texte')
							))
							->set_header_template( '<%- tabname %>' )
					))

					->add_fields('chiffres',"Module chiffres", array(
						Field::make('text', 'title', 'Titre'),
						Field::make('textarea', 'intro', 'Introduction'),
						Field::make('complex', 'chiffres', 'Chiffres')
							->set_collapsed(true)
							->setup_labels(array('plural_name' => 'Chiffres', 'singular_name' => 'Chiffre'))
							->add_fields(array(
								Field::make('text', 'chiffre', 'Chiffre')
									->set_width(50),
								Field::make('textarea', 'desc', 'Description')
									->set_width(50),
							))
							->set_header_template( '<%- chiffre %>' )
					))

					->add_fields('map',"Module Map", array(
						Field::make( 'separator', 'map-noused', 'Map affichée sur la page' )
					))
		/*	// Module : video with quote
			->add_fields('videoquote', 'Module Video & citation', array(
				Field::make('text', 'quote', 'Citation (optionnel)'),
				Field::make('text', 'youtube_key', 'Clef vidéo youtube')
			))

			// Module : mutiblocks
			->add_fields('multiblocks', 'Module Multiples Blocs', array(
				Field::make('text', 'text_title', 'Texte du titre (optionnel)'),
				Field::make('complex', 'blocks', 'Blocs')
					->set_collapsed(true)
					->setup_labels(array('plural_name' => 'Blocs', 'singular_name' => 'Blocs'))
					->add_fields(array(
						Field::make('text', 'title', 'Titre du  bloc')
							->set_width(50),
						Field::make('textarea', 'text', 'Texte du  bloc')
							->set_width(50),
					))
					->set_header_template( '<%- title %>' )
			))

			// Module : logo caroussel
			->add_fields('logocaroussel', 'Module Logos caroussel', array(
				Field::make('text', 'title', 'Titre'),
				Field::make('media_gallery', 'logo', 'Logos')
			))

			// Module : Members
			->add_fields('members', 'Module Membres', array(
				Field::make('text', 'title', 'Titre'),
				Field::make('complex', 'members', 'Members')
				->set_collapsed(true)
				->setup_labels(array('plural_name' => 'Members', 'singular_name' => 'Member'))
				->add_fields(array(
					Field::make("text", "name", "Nom")
						->set_width(50),
					Field::make( 'image', 'photo', 'Photo' )
						->set_value_type( 'url' )
						->set_width(50),
					Field::make("text", "job", "Job")
						->set_width(50),
					Field::make( 'checkbox', 'management', 'Membre de la direction' ),
				))
				->set_header_template( '<%- name %>' )
			))

			// Module : FAQ
			->add_fields('faq', 'Module FAQ', array(
				Field::make('text', 'title', 'Titre'),
				Field::make('complex', 'questions', 'Questions')
				->set_collapsed(true)
				->setup_labels(array('plural_name' => 'Questions', 'singular_name' => 'Question'))
				->add_fields(array(
					Field::make("text", "question", "Question"),
					Field::make("textarea", "answer", "Réponse")
				))
				->set_header_template( '<%- question %>' )
			))

			//Module : Reseau
			->add_fields('reseau', 'Module Reseau', array(
				Field::make('text', 'title', 'Reseau title'),
				Field::make('text', 'link_text', 'Texte du lien ')
				->set_width(50),
				Field::make('text', 'link_url', 'Url du lien')
					->set_width(50),
				Field::make('complex', 'adresses', 'Adresses')
				->set_collapsed(true)
				->setup_labels(array('plural_name' => 'Adresses', 'singular_name' => 'Adresse'))
				->add_fields(array(
					Field::make('text', 'name', "Nom de l'adresse"),
					Field::make('text', 'adresse', 'Adresse'),
					Field::make('text', 'phone', 'Numéro de téléphone')
				))
				->set_header_template( '<%- name %>' )
			))*/
	));

/*------------------------------------*\
	CONTACT
\*------------------------------------*/
Container::make("post_meta", "Information de contact")
->show_on_post_type('page')
->show_on_template('template-contact.php')
->add_fields( array(
	Field::make('text', 'info_contact_title', 'Sous-titre'),
	Field::make('complex', 'contact_locations', 'Bureaux')
	->set_collapsed(true)
	->setup_labels(array('plural_name' => 'Blocs', 'singular_name' => 'Bureau'))
	->add_fields(array(
		Field::make('text', 'title', 'Nom du bureau')
			->set_width(50),
		Field::make('textarea', 'text', 'Informations du bureau')
			->set_width(50)
			->set_help_text("Les retours à la lignes sont pris en comptes"),
	))
	->set_header_template( '<%- title %>' ),
	Field::make('text', 'contact_form_title', 'Titre du formulaire'),
	Field::make( 'image', 'contact_img', 'Image de background'),
	Field::make('text', 'contact_cf7', 'Code du formulaire cf7'),
));

/*------------------------------------*\
	Footer Theme Options -- FR
\*------------------------------------*/
Container::make( 'theme_options', __( 'Footer FR' ) )
	->set_icon( 'dashicons-welcome-widgets-menus' )
	->add_fields( array(

		Field::make( 'separator', 'footer_address_separator_fr', __( 'Adresse' ) ),
		Field::make( 'text', 'footer_address_title_fr', 'Titre "Siège social"' )
			->set_width(50),
		Field::make( 'textarea', 'footer_address_desc_fr', 'Description "Siège social"' )
			->set_width(50),

		Field::make( 'separator', 'footer_nums_separator_fr', __( 'Numéros' ) ),
		Field::make( 'text', 'footer_nums_title_fr', 'Titre "Contacts des centres"' )
			->set_width(33),
		Field::make( 'text', 'footer_nums_desc1_fr', 'Texte "Choisir un centre pour voir son numéro."' )
			->set_width(33),
		Field::make( 'text', 'footer_nums_desc2_fr', 'Texte "Numéro de Téléphone"' )
			->set_width(33),
		Field::make('complex', 'footer_nums_list_fr', 'Numéros')
			->set_collapsed(true)
			->setup_labels(array('plural_name' => 'Numéros', 'singular_name' => 'Numéro'))
			->add_fields(array(
				Field::make('text', 'text', 'Localisation ')
					->set_width(50),
				Field::make('text', 'number', 'Numéro')
					->set_width(50),
			))
			->set_header_template( '<%- text %>' ),

		Field::make( 'separator', 'footer_group_separator_fr', __( 'Le groupe' ) ),
		Field::make( 'text', 'footer_group_title_fr', 'Titre "Le groupe"' )
			->set_width(33),
		Field::make('complex', 'footer_group_list_fr', 'Liens')
			->set_collapsed(true)
			->setup_labels(array('plural_name' => 'Liens', 'singular_name' => 'Lien'))
			->add_fields(array(
				Field::make('text', 'title', 'Texte ')
					->set_width(50),
				Field::make('text', 'link', 'Lien')
					->set_width(50),
			))
			->set_header_template( '<%- title %>' ),
	));



/*------------------------------------*\
	Footer Theme Options -- EN
\*------------------------------------*/
Container::make( 'theme_options', __( 'Footer EN' ) )
	->set_icon( 'dashicons-welcome-widgets-menus' )
	->add_fields( array(

		Field::make( 'separator', 'footer_address_separator_en', __( 'Adresse' ) ),
		Field::make( 'text', 'footer_address_title_en', 'Titre "Siège social"' )
			->set_width(50),
		Field::make( 'textarea', 'footer_address_desc_en', 'Description "Siège social"' )
			->set_width(50),

		Field::make( 'separator', 'footer_nums_separator_en', __( 'Numéros' ) ),
		Field::make( 'text', 'footer_nums_title_en', 'Titre "Contacts des centres"' )
			->set_width(33),
		Field::make( 'text', 'footer_nums_desc1_en', 'Texte "Choisir un centre pour voir son numéro."' )
			->set_width(33),
		Field::make( 'text', 'footer_nums_desc2_en', 'Texte "Numéro de Téléphone"' )
			->set_width(33),
		Field::make('complex', 'footer_nums_list_en', 'Numéros')
			->set_collapsed(true)
			->setup_labels(array('plural_name' => 'Numéros', 'singular_name' => 'Numéro'))
			->add_fields(array(
				Field::make('text', 'text', 'Localisation ')
					->set_width(50),
				Field::make('text', 'number', 'Numéro')
					->set_width(50),
			))
			->set_header_template( '<%- text %>' ),

		Field::make( 'separator', 'footer_group_separator_en', __( 'Le groupe' ) ),
		Field::make( 'text', 'footer_group_title_en', 'Titre "Le groupe"' )
			->set_width(33),
		Field::make('complex', 'footer_group_list_en', 'Liens')
			->set_collapsed(true)
			->setup_labels(array('plural_name' => 'Liens', 'singular_name' => 'Lien'))
			->add_fields(array(
				Field::make('text', 'title', 'Texte ')
					->set_width(50),
				Field::make('text', 'link', 'Lien')
					->set_width(50),
			))
			->set_header_template( '<%- title %>' ),


	));





/*------------------------------------*\
	404
\*------------------------------------*/
Container::make('theme_options', __( 'Page 404' ) )
->add_fields( array(
	Field::make( 'text', 'th_404_text_fr', 'Message (FR)' )
	->set_required( false )
	->set_width(50),
	Field::make( 'text', 'th_404_text_en', 'Message (EN)' )
	->set_required( false )
	->set_width(50),
	Field::make( 'text', 'th_404_btn_fr', 'Texte du bouton "retour à l\'accueil" (FR)' )
	->set_required( false )
	->set_width(50),
	Field::make( 'text', 'th_404_btn_en', 'Texte du bouton "retour à l\'accueil" (EN)' )
	->set_required( false )
	->set_width(50),
) );
