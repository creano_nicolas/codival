<?php /* Template Name: Template - contact */
get_header();
$subtitle = carbon_get_the_post_meta('info_contact_title');
$locations = carbon_get_the_post_meta('contact_locations');
$code_cf7 = carbon_get_the_post_meta('contact_cf7');
$formtitle = carbon_get_the_post_meta('contact_form_title');
$img = wp_get_attachment_image_src(carbon_get_the_post_meta('contact_img'), 'large')[0];
?>


<div class="page-container" data-slug="contact">
		<section class="page-content">
			<header class="page-content--header content-container content-container__sm">
				<div class="breadcrumb">
					<?php get_breadcrumb(); ?>
				</div>
				<div class="sqtitle">
					<div class="sqtitle--in">
						<div class="decoangle decoangle__orange decoangle__tr decoangle__sm"></div>
						<div class="decoangle decoangle__orange decoangle__bl decoangle__md"></div>
						<h1 class="sqtitle--title"><?php the_title(); ?></h1>
					</div>
				</div>
			</header>
		<section class="content-container">
				<h2 class="title-section"><?= $subtitle ?></h2>
				<div class="contact-locations">
					<?php foreach($locations as $location): ?>
						<div class="contact-locations__item">
							<h4><?= $location["title"] ?></h4>
							<p><?= $location["text"] ?></p>
						</div>
					<?php endforeach; ?>
				</div>
		</section>
		<section class="contact-form-section" style="background-image: url('<?= $img ?>')">
			<div class="content-container">
				<h2 class="title-section"><?= $formtitle ?></h2>
			</div>
			<div class="content-container content-container__sm">
				<?php echo do_shortcode($code_cf7); ?>
			</div>
		</section>
	</section>
</div>

<?php get_footer(); ?>
