<?php /* Template Name: Template - home */ ?>
<?php get_header(); ?>
<div class="page-container" data-slug="all">
	<section class="page-content">
		<?php
			$module = [
				"slides" => carbon_get_the_post_meta('home_behead_slides')
			];
			include(locate_template('modules/module-bhead.php'));

			$module = [
				"title_tl" => carbon_get_the_post_meta('home_codibest_title_tl'),
				"title" => carbon_get_the_post_meta('home_codibest_title'),
				"desc" => carbon_get_the_post_meta('home_codibest_desc'),
				"link" => carbon_get_the_post_meta('home_codibest_link'),
				"link_text" => carbon_get_the_post_meta('home_codibest_link_text'),
			];
			include(locate_template('modules/module-codibest.php'));

			$module = [
				"title_tl" => carbon_get_the_post_meta('home_codiacti_title_tl'),
				"title" => carbon_get_the_post_meta('home_codiacti_title'),
				"desc" => carbon_get_the_post_meta('home_codiacti_desc'),
				"slides" => carbon_get_the_post_meta('home_codiacti_slides'),
			];
			include(locate_template('modules/module-acti.php'));

			$module = [
				"title_tl" => carbon_get_the_post_meta('home_codires_title_tl'),
				"title" => carbon_get_the_post_meta('home_codires_title'),
				"desc" => carbon_get_the_post_meta('home_codires_desc'),
				"slides" => carbon_get_the_post_meta('home_codires_slides'),
			];
			include(locate_template('modules/module-codires.php'));
		?>
	</section>
</div>
<?php get_footer(); ?>
