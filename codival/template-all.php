<?php
/*
Template Name: Template - static components
*/
get_header();
?>
<div class="page-container" data-slug="all">
	<section class="page-content">

		<div class="bhead">
			<div class="bhead--deco">
				<div class="decoangle decoangle__green decoangle__tr decoangle__lg"></div>
				<div class="decoangle decoangle__orange decoangle__bl decoangle__bg"></div>
				<div class="bhead--deco-circ"><?php get_template_part('svg/big-circle-header'); ?></div>
			</div>
			<div class="bhead--bgslide">
				<div style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/footer-background.png);"></div>
				<div style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/livreur.jpg);"></div>
			</div>
			<div class="bhead--slides">
				<div class="bhead--slide">
					<div class="bhead--title">Découvrez notre solution de coffre-fort intelligent</div>
					<div class="bhead--cta">
						<a href="#" class="arcta arcta__ccfff arcta__arrow">
							<span class="arcta--text">En savoir plus</span>
							<span class="arcta--arrow"><?php get_template_part('svg/button-arrow'); ?></span>
						</a>
					</div>
				</div>
				<div class="bhead--slide">
					<div class="bhead--title">Foobar lorem ipsum</div>
					<div class="bhead--cta">
						<a href="#" class="arcta arcta__ccfff arcta__arrow">
							<span class="arcta--text">En savoir plus</span>
							<span class="arcta--arrow"><?php get_template_part('svg/button-arrow'); ?></span>
						</a>
					</div>
				</div>
			</div>
			<div class="bhead--nav">
				<div class="bhead--prev arrowcircle">
					<?php get_template_part('svg/arrow-circle-left'); ?>
				</div>
				<div class="bhead--next arrowcircle">
					<?php get_template_part('svg/arrow-circle-right'); ?>
				</div>
			</div>
		</div>

		<div class="codibest">
      <?php
      	$timeline = array(
          "stroke_color" 		=> "white",
          "out"     				=> "orange",
          "in"      				=> "white",
          "text_color" 			=> "white",
          "text"    				=> "Codival",
      	);
      	include(locate_template('modules/module-timeline.php'));
      ?>
			<div class="codibest--bcenter">
				<div class="sqtitle">
					<div class="sqtitle--in">
						<div class="decoangle decoangle__orange decoangle__tr decoangle__sm"></div>
						<div class="decoangle decoangle__orange decoangle__bl decoangle__md"></div>
						<div class="sqtitle--title">Le meilleur de la sécurisation de valeurs</div>
					</div>
				</div>
				<div class="codibest--desc">
					<p>Créée en 1975 à l'initiative des Banques de la place, qui voulaient sécuriser les Transports de Fonds sur leurs réseaux grandissant. CODIVAL est leader en Côte d'Ivoire dans le domaine du Transport de Fonds, Traitement de Valeurs et gestion des Automates Bancaires.</p>
				</div>

				<a href="#" class="arcta arcta__ccfff arcta__arrow">
					<span class="arcta--text">En savoir plus</span>
					<span class="arcta--arrow"><?php get_template_part('svg/button-arrow'); ?></span>
				</a>
			</div>
		</div>

		<div class="acti">
      <?php
      	$timeline = array(
          "stroke_color" 		=> "green",
          "out"     				=> "orange",
          "in"      				=> "green",
          "text_color" 			=> "green",
          "text"    				=> "Nos&nbsp;activités",
      	);
      	include(locate_template('modules/module-timeline.php'));
      ?>
			<div class="acti--bg">
				<?php include(locate_template('svg/fade-logo.php')); ?>
			</div>
      <div class="acti--bcenter">
      	<div class="acti--head">
					<div class="sqtitle">
						<div class="sqtitle--in">
							<div class="decoangle decoangle__orange decoangle__tr decoangle__sm"></div>
							<div class="decoangle decoangle__orange decoangle__bl decoangle__md"></div>
							<div class="sqtitle--title">Des activités sur <br>tout le territoire</div>
						</div>
					</div>
					<div class="acti--desc">
						<p>Découvrez nos services qui garantissent la protection de vos valeurs.</p>
					</div>
				</div>
			</div>
			<div class="acti--sscont">
				<div class="basicss basicss__nooverflow">
					<div class="basicss--nav">
						<div class="basicss--prev arrowcircle">
							<?php get_template_part('svg/arrow-circle-left'); ?>
						</div>
						<div class="basicss--next arrowcircle">
							<?php get_template_part('svg/arrow-circle-right'); ?>
						</div>
					</div>
					<div class="basicss--slider">
						<?php for ($i=0; $i < 4; $i++) : ?>
							<div class="basicss--item">
								<div class="acticard">
									<div class="acticard--head">
										<div class="acticard--imgc">
											<img class="img-cover" data-object-fit="cover" src="<?php echo get_template_directory_uri(); ?>/assets/img/livreur.jpg);">
										</div>
										<div class="acticard--headline">
											<h2 class="acticard--title">Transports de fonds et de valeurs</h2>
										</div>
									</div>
									<div class="acticard--desc">
										<p>Depuis 1975, CODIVAL  partage avec ses clients, un savoir-faire unique en matière de convoi de fonds. Ils bénéficient ainsi, d'une offre sur mesure pour une grande variété de valeurs.</p>
									</div>
									<div class="acticard--ctac">
										<a href="#" class="arcta arcta__cc000 arcta__arrow">
											<span class="arcta--text">En savoir plus</span>
											<span class="arcta--arrow"><?php get_template_part('svg/button-arrow'); ?></span>
										</a>
									</div>
								</div>
							</div>
						<?php endfor; ?>
					</div>
				</div>
			</div>
		</div>

		<div class="codires">
      <?php
      	$timeline = array(
          "stroke_color" 		=> "green",
          "out"     				=> "orange",
          "in"      				=> "green",
          "text_color" 			=> "green",
          "text"    				=> "Le&nbsp;réseau codival",
      	);
      	include(locate_template('modules/module-timeline.php'));
      ?>
			<div class="codires--bg">
				<?php include(locate_template('svg/africa-graphism.php')); ?>
			</div>
      <div class="codires--bcenter">
      	<div class="codires--head">
					<div class="sqtitle">
						<div class="sqtitle--in">
							<div class="decoangle decoangle__orange decoangle__tr decoangle__sm"></div>
							<div class="decoangle decoangle__orange decoangle__bl decoangle__md"></div>
							<div class="sqtitle--title">Membre d’un réseau <br>international</div>
						</div>
					</div>
					<div class="codires--desc">
						<p>CODIVAL, filiale du Groupe SAGAM International leader dans la sous-région Ouest Africaine pour toutes les activités liées à la sécurité:
						<br><b>Transport de Fonds,  Traitement des Valeurs, Gestion des Automates Bancaires,  Sécurité Électronique et Incendie.</b></p>
					</div>
				</div>
			</div>
			<div class="codires--sscont">
				<div class="basicss basicss__nooverflow">
					<div class="basicss--nav">
						<div class="basicss--prev arrowcircle">
							<?php get_template_part('svg/arrow-circle-left'); ?>
						</div>
						<div class="basicss--next arrowcircle">
							<?php get_template_part('svg/arrow-circle-right'); ?>
						</div>
					</div>
					<div class="basicss--slider">
						<?php for ($i=0; $i < 4; $i++) : ?>
							<div class="basicss--item">
								<div class="rescard">
									<div class="rescard--logoc">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/coditrans.svg">
									</div>
									<div class="rescard--infos">
										<div class="rescard--title">CODITRANS</div>
										<div class="rescard--desc">
											Abidjan, Rue du canal, Zone 4 C
											<br>15 BP 503  Abidjan 15
										</div>
										<div class="rescard--contact">
											Tél. (225) 21.75.70.60
											<br>Fax. (225) 21.75.70.70
										</div>
									</div>
								</div>
							</div>
						<?php endfor; ?>
					</div>
				</div>
			</div>
		</div>

    <?php /*
    <div class="content-container content-container__sm">
			<div class="breadcrumb">
				<?php get_breadcrumb(); ?>
			</div>
		</div>

		<header class="page-content--header content-container content-container__sm">
			<h1 class="title-page"><?php the_title(); ?></h1>
		</header>

	<!-- 1er Module -->
		<div class="ct-slider ct-slider__incontent ct-slider__m-spaces">
			<div>
				<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/interieur-appart-luxembourg-2.jpg">
			</div>
			<div>
				<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/plan-appart-luxembourg-1.jpg">
			</div>
			<div>
				<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/plan-appart-luxembourg.png">
			</div>
		</div>

	<!-- 2ème Module -->
		<div class="page-content--container">
			<h2 class="section-title">Modernité et patrimoine</h2>
			<div class="markup markup__2cols">
				<p class="fs-24">
					<span style="line-height: 1.5;">
						Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.
					</span>
				</p>
				<p class="fs-18">
					Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.
				</p>
				<a href="#"class="btn__invert">
					<span class="btn--content">Télécharger la fiche projet</span>
					<span class="btn--arrow"></span>
				</a>
			</div>
		</div>

	<!-- 3ème Module -->
		<section class="page-content--section page-content--section__full-width page-content--section__color-blue deco-bg scroll-reveal">
			<div class="content-container content-container__sm">
				<div class="markup">
					<blockquote>
					Nous maintenons la riche biodiveristé du milieu, et l'écosystème des bas-fond.
				</blockquote>
				</div>

				<div class="video-wrapper">
					<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/mX7Te0ggYak" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			<!-- /.content-container -->
		</section>

		<!-- 4ème Module -->
		<div class="page-content--container page-content--container__m">

			<h2 class="section-title">Note Architecturale</h2>

			<div class="markup descriptions">
				<p>Les ouvertures sont généreuses, les terrasses, jardins et loggias sont considérés comme de vrais espaces à vivre ou à partager...<br />Si le pouvoir d’achat aujourd’hui ne permet plus de réaliser de grandes surfaces, il est important de pouvoir proposer des surfaces complémentaires accompagnant l’espace de vie : jardin, balcon, rangements, locaux vélos, espaces de vie partagés et services complémentaires ...<br />La réflexion architecturale a été menée afin que le projet respecte une densité urbaine répondant aux besoins grandissants de la ville, tout en créant des espaces de rencontre et en préservant l’intimité de chaque logement.<br />Les îlots dialoguent ensemble par leur esthétique. Tonalités et matériaux font le lien entre les îlots. Les porosités organisées en RDC et la composition paysagère renforcent les communications. Les îlots se complètent par le panel des différentes typologies de logements et par la proposition de  services et commerces.</p><p>Le vivre ensemble est encouragé par :</p><ul><li>Des immeubles de petite taille, peu gourmands en énergie, offrant :<br /><span style="line-height: 1.5;">- une flexibilité de composition, pour la prise en compte de besoins des futurs occupants.<br /></span><span style="line-height: 1.5;">- une organisation architecturale et spatiale facilitant les échanges entre générations.<br /></span>- Des habitants solidaires, adhérents d’une association de résidence et unis par une charte du Vivre Ensemble. Un concept qui valorise la mixité intergénérationnelle et stimule le lien social par le voisinage actif et la participation à des clubs et l’échange de services.</li><li><span style="line-height: 1.5;">Des économies pour les résidents : une réduction des charges de l’ordre de 30 %, grâce aux actions menées par les résidents dans le cadre des clubs (ménage, entretien des espaces verts...) et aux performances techniques des constructions.</span></li><li>Un système structuré : une gouvernance associative et participative, garante de son bon fonctionnement, rassemblant tous les acteurs locaux.</li><li>Un concept réversible : en cas de difficultés dans la mise en œuvre du concept, la résidence retrouve un fonctionnement classique d’habitat collectif.</li><li>Une diversité architecturale favorisée par l’implication des habitants dans la démarche de mutualisation des espaces et par l’animation des parties communes.</li></ul><p>L'ouverture du grand site au droit du bassin chambérien offre des vues exceptionnelles, que ce soit au Nord vers le Nivolet, ou au Sud vers le Granier.<br />Le schéma directeur proposé, notamment dans les orientations des futurs immeubles permet de valoriser les logements par des cadrages soignés sur ces émergences caractéristiques du paysage lointain.</p><ul><li>Un ensemble bâti ouvert sur le centre bourg par une mise en scène de la voie structurante soigneusement positionnée.</li><li>Des perspectives visuelles depuis les RDC des nouveaux immeubles pour réussir le dialogue entre le centre bourg et le Parc Habité.</li></ul>
			</div>
		</div>

	<!-- 5ème Module
		<section class="page-content--section scroll-reveal">
			<div class="content-container content-container__sm">
				<h2 class="title-section pbs">Nos valeurs</h2>
			</div>
			<div class="content-container">
				<div class="keywords-list">
					<ul class="grid">
						<li class="keywords-list--item col-4_xs-8">
							<span class="keywords-list--item--title">Passion</span>
							<p class="txt-smaller">Le goût de partager et de transmettre la passion des métiers de la joaillerie.</p>
						</li>

						<li class="keywords-list--item col-4_xs-8">
							<span class="keywords-list--item--title">Transmission</span>
							<p class="txt-smaller">Un savoir-faire séculaire d’exception, un patrimoine français à préserver et à transmettre de génération en génération.</p>
						</li>

						<li class="keywords-list--item col-4_xs-8">
							<span class="keywords-list--item--title">Création</span>
							<p class="txt-smaller">Révéler la créativité singulière de chacun et réinventer des parcours pour anticiper les changements.</p>
						</li>

						<li class="keywords-list--item col-4_xs-8">
							<span class="keywords-list--item--title">Exigence</span>
							<p class="txt-smaller">L’engagement au service de l’excellence pédagogique garant d’une insertion professionnelle réussie. </p>
						</li>

						<li class="keywords-list--item col-4_xs-8">
							<span class="keywords-list--item--title">Dynamisme</span>
							<p class="txt-smaller">Un modèle d’école ouverte en prise avec les mutations de notre monde.</p>
						</li>

						<li class="keywords-list--item col-4_xs-8">
							<span class="keywords-list--item--title">Innovation</span>
							<p class="txt-smaller">Une veille permanente pour adapter les méthodes et les cursus d’enseignement aux évolutions technologiques et aux besoins de la filière. </p>
						</li>

						<li class="keywords-list--item col-4_xs-8">
							<span class="keywords-list--item--title">Ouverture</span>
							<p class="txt-smaller">Le garant d’un patrimoine, d’un art de vivre français, d’un état d’esprit universel tourné vers le futur.</p>
						</li>
					</ul>
				</div>
			</div>
		</section> -->

	<!-- 6ème Module -->
		<!-- <section class="page-content--section scroll-reveal">
			<div class="content-container content-container__sm">
				<div class="markup">
					<h2>LA HAUTE ECOLE DE JOAILLERIE</h2><h3>UNE ÉCOLE FAITE PAR LE MÉTIER POUR LE MÉTIER</h3><p>Plus ancien établissement de joaillerie au monde, la Haute Ecole de Joaillerie est créée en 1867 par le métier. Son lien filial avec la profession en fait un des piliers de la transmission du savoir-faire français. Depuis 150 ans, la Haute école de Joaillerie a formé des générations de bijoutiers, joailliers, orfèvres, gemmologues qui exercent dans les plus grands ateliers. Véritable référence pour tous les acteurs de la profession, elle regroupe quatre départements : la formation initiale, la formation en alternance, la formation professionnelle continue et la formation internationale.</p><p>Certifiée ISO 9001, la Haute école de Joaillerie offre à l’ensemble des apprenants des ateliers et des équipements haut de gamme pour un apprentissage complet des métiers de la Bijouterie-Joaillerie, de la transmission du geste aux technologies les plus modernes de conception et dessin par ordinateur.</p>				</div>
			</div>
		</section> -->



	<!-- 8ème Module
		<section id="listing" class="page-content--section scroll-reveal">
			<div class="content-container">
				<h2 class="title-section">Nos actualités</h2>
				<ul class="list-media-filter-nav">

					<li class="list-media-filter-nav--title">Filtrer par&nbsp;:</li>

					<li class="list-media-filter-nav--item list-media-filter-nav--item__current">
						<a href="https://hauteecoledejoaillerie.com/actualites/#listing"><span>Toutes</span></a>
					</li>

					<li class="list-media-filter-nav--item" data-theme-formation="continue">
						<a href="https://hauteecoledejoaillerie.com/departement-formation-continue/#listing"><span>Formation continue</span></a>
					</li>

					<li class="list-media-filter-nav--item" data-theme-formation="alternance">
						<a href="https://hauteecoledejoaillerie.com/departement-formation-en-alternance/#listing"><span>Formation en alternance</span></a>
					</li>

					<li class="list-media-filter-nav--item" data-theme-formation="initiale">
						<a href="https://hauteecoledejoaillerie.com/departement-formation-initiale/#listing"><span>Formation initiale</span></a>
					</li>

					<li class="list-media-filter-nav--item" data-theme-formation="neutre">
						<a href="https://hauteecoledejoaillerie.com/portes-ouvertes/#listing"><span>Portes ouvertes</span></a>
					</li>

					<li class="list-media-filter-nav--item" data-theme-formation="neutre">
						<a href="https://hauteecoledejoaillerie.com/ecole/#listing"><span>Vie de l'Ecole</span></a>
					</li>
				</ul>

				<div class="list-media-wrapper">
					<ul class="list-media grid">
						<li class="list-media--item col-8_xs-16" data-theme-formation="initiale">
							<a href="https://hauteecoledejoaillerie.com/departement-formation-initiale/alumni-hej-inscrivez-vous-a-lassociation-des-anciens-eleves-de-la-haute-ecole-de-joaillerie/">
								<div class="list-media--item--picture">
									<img src="https://hauteecoledejoaillerie.com/wp-content/uploads/2019/10/photo-14-1-768x312.jpg" alt="">
								</div>
							</a>
							<span class="list-media-item--meta"><span class="list-media-item--cat">Formation initiale</span>&nbsp;|&nbsp;<span class="list-media-item--date">22.10.2019</span></span>
							<a href="https://hauteecoledejoaillerie.com/departement-formation-initiale/alumni-hej-inscrivez-vous-a-lassociation-des-anciens-eleves-de-la-haute-ecole-de-joaillerie/">
								<h2 class="list-media--item--title">Alumni HEJ : inscrivez-vous à l’association des anciens élèves de la Haute École de Joaillerie</h2>
							</a>
						</li>

						<li class="list-media--item col-8_xs-16" data-theme-formation="initiale">
							<a href="https://hauteecoledejoaillerie.com/departement-formation-initiale/projet-individuel-de-creation-des-eleves-de-bachelor-3-design-et-creation-avec-la-maison-christofle/">
								<div class="list-media--item--picture">
									<img src="https://hauteecoledejoaillerie.com/wp-content/uploads/2019/10/DSC_0122-768x312.jpg" alt="">
								</div>
							</a>
							<span class="list-media-item--meta"><span class="list-media-item--cat">Formation initiale</span>&nbsp;|&nbsp;<span class="list-media-item--date">18.10.2019</span></span>
							<a href="https://hauteecoledejoaillerie.com/departement-formation-initiale/projet-individuel-de-creation-des-eleves-de-bachelor-3-design-et-creation-avec-la-maison-christofle/">
								<h2 class="list-media--item--title">Projet Individuel de Création des élèves de Bachelor 3  Design et Création, avec la maison Christofle</h2>
							</a>
						</li>

						<li class="list-media--item col-8_xs-16" >
							<a href="https://hauteecoledejoaillerie.com/ecole/ceremonie-de-rentree-de-la-haute-ecole-de-joaillerie-a-paris/">
								<div class="list-media--item--picture">
									<img src="https://hauteecoledejoaillerie.com/wp-content/uploads/2019/10/photo-13-768x312.jpg" alt="">
								</div>
							</a>
							<span class="list-media-item--meta"><span class="list-media-item--cat">Vie de l'Ecole</span>&nbsp;|&nbsp;<span class="list-media-item--date">11.10.2019</span></span>
							<a href="https://hauteecoledejoaillerie.com/ecole/ceremonie-de-rentree-de-la-haute-ecole-de-joaillerie-a-paris/">
								<h2 class="list-media--item--title">Cérémonie de rentrée de la Haute Ecole de Joaillerie à Paris</h2>
							</a>
						</li>

						<li class="list-media--item col-8_xs-16" data-theme-formation="initiale">
							<a href="https://hauteecoledejoaillerie.com/departement-formation-initiale/cap-en-cours-du-soir-profitez-des-tarifs-de-lancement-du-cap/">
								<div class="list-media--item--picture">
									<img src="https://hauteecoledejoaillerie.com/wp-content/uploads/2019/10/HEJ-2019-Cours-Adultes-1005-1-768x312.jpg" alt="">
								</div>
							</a>
							<span class="list-media-item--meta"><span class="list-media-item--cat">Formation initiale</span>&nbsp;|&nbsp;<span class="list-media-item--date">08.10.2019</span></span>
							<a href="https://hauteecoledejoaillerie.com/departement-formation-initiale/cap-en-cours-du-soir-profitez-des-tarifs-de-lancement-du-cap/">
								<h2 class="list-media--item--title">Cap en cours du soir profitez des tarifs de lancement !</h2>
							</a>
						</li>

						<li class="list-media--item col-8_xs-16" >
							<a href="https://hauteecoledejoaillerie.com/ecole/rentree-2019-bienvenue-a-nos-nouveaux-eleves/">
								<div class="list-media--item--picture">
									<img src="https://hauteecoledejoaillerie.com/wp-content/uploads/2019/09/DSC_0618-768x312.jpg" alt="">
								</div>
							</a>
							<span class="list-media-item--meta"><span class="list-media-item--cat">Vie de l'Ecole</span>&nbsp;|&nbsp;<span class="list-media-item--date">09.09.2019</span></span>
							<a href="https://hauteecoledejoaillerie.com/ecole/rentree-2019-bienvenue-a-nos-nouveaux-eleves/">
								<h2 class="list-media--item--title">Rentrée 2019 : bienvenue à nos nouveaux élèves</h2>
							</a>
						</li>

						<li class="list-media--item col-8_xs-16" data-theme-formation="alternance">
							<a href="https://hauteecoledejoaillerie.com/departement-formation-en-alternance/le-cfa-de-la-haute-ecole-de-joaillerie-a-lhonneur-dans-le-jt-de-france-3/">
								<div class="list-media--item--picture">
									<img src="https://hauteecoledejoaillerie.com/wp-content/uploads/2019/09/EDtiI7NXYAAEsEf-680x312.png" alt="">
								</div>
							</a>
							<span class="list-media-item--meta"><span class="list-media-item--cat">Formation en alternance</span>&nbsp;|&nbsp;<span class="list-media-item--date">06.09.2019</span></span>
							<a href="https://hauteecoledejoaillerie.com/departement-formation-en-alternance/le-cfa-de-la-haute-ecole-de-joaillerie-a-lhonneur-dans-le-jt-de-france-3/">
								<h2 class="list-media--item--title">Le CFA de la Haute Ecole de Joaillerie à l&rsquo;honneur dans le JT de France 3</h2>
							</a>
						</li>

						<li class="list-media--item col-8_xs-16" data-theme-formation="initiale">
							<a href="https://hauteecoledejoaillerie.com/departement-formation-initiale/ceremonie-de-remise-du-certificat-superieur-de-joaillerie-de-la-promotion-chanel/">
								<div class="list-media--item--picture">
									<img src="https://hauteecoledejoaillerie.com/wp-content/uploads/2019/07/20190703_182150-768x312.jpg" alt="">
								</div>
							</a>
							<span class="list-media-item--meta"><span class="list-media-item--cat">Formation initiale</span>&nbsp;|&nbsp;<span class="list-media-item--date">24.07.2019</span></span>
							<a href="https://hauteecoledejoaillerie.com/departement-formation-initiale/ceremonie-de-remise-du-certificat-superieur-de-joaillerie-de-la-promotion-chanel/">
								<h2 class="list-media--item--title">Cérémonie de remise du Certificat Supérieur de Joaillerie de la promotion Chanel</h2>
							</a>
						</li>

						<li class="list-media--item col-8_xs-16" data-theme-formation="continue">
							<a href="https://hauteecoledejoaillerie.com/departement-formation-continue/reunions-dinformation-decouvrez-nos-cursus-de-formation-en-bijouterie-joaillerie-et-gemmologie-2/">
								<div class="list-media--item--picture">
									<img src="https://hauteecoledejoaillerie.com/wp-content/uploads/2019/07/HEJ-2019-Atelier-Joaillerie-BMA2-803-768x312.jpg" alt="">
								</div>
							</a>
							<span class="list-media-item--meta"><span class="list-media-item--cat">Formation continue</span>&nbsp;|&nbsp;<span class="list-media-item--date">24.07.2019</span></span>
							<a href="https://hauteecoledejoaillerie.com/departement-formation-continue/reunions-dinformation-decouvrez-nos-cursus-de-formation-en-bijouterie-joaillerie-et-gemmologie-2/">
								<h2 class="list-media--item--title">Réunions d’information : découvrez nos cursus de formation en bijouterie, joaillerie et gemmologie</h2>
							</a>
						</li>

						<li class="list-media--item col-8_xs-16" data-theme-formation="continue">
							<a href="https://hauteecoledejoaillerie.com/departement-formation-continue/formation-continue-remise-des-diplomes-du-cap-et-de-la-mention-complementaire-2019/">
								<div class="list-media--item--picture">
									<img src="https://hauteecoledejoaillerie.com/wp-content/uploads/2019/07/DSC_0697-768x312.jpg" alt="">
								</div>
							</a>
							<span class="list-media-item--meta"><span class="list-media-item--cat">Formation continue</span>&nbsp;|&nbsp;<span class="list-media-item--date">18.07.2019</span></span>
							<a href="https://hauteecoledejoaillerie.com/departement-formation-continue/formation-continue-remise-des-diplomes-du-cap-et-de-la-mention-complementaire-2019/">
								<h2 class="list-media--item--title">Formation Continue : remise des diplômes du CAP et de la Mention Complémentaire 2019</h2>
							</a>
						</li>

						<li class="list-media--item col-8_xs-16" >
							<a href="https://hauteecoledejoaillerie.com/ecole/haute-ecole-de-joaillerie-remarquables-taux-de-reussite-aux-examens/">
								<div class="list-media--item--picture">
									<img src="https://hauteecoledejoaillerie.com/wp-content/uploads/2019/07/HEJ-2019-Atelier-Joaillerie-BMA2-803-768x312.jpg" alt="">
								</div>
							</a>
							<span class="list-media-item--meta"><span class="list-media-item--cat">Vie de l'Ecole</span>&nbsp;|&nbsp;<span class="list-media-item--date">11.07.2019</span></span>
							<a href="https://hauteecoledejoaillerie.com/ecole/haute-ecole-de-joaillerie-remarquables-taux-de-reussite-aux-examens/">
								<h2 class="list-media--item--title">Haute Ecole de Joaillerie : remarquables taux de réussite aux examens</h2>
							</a>
						</li>
					</ul>
				</div>

				<div class="pagination">
					<ul class='page-numbers'>
						<li><span aria-current='page' class='page-numbers current'>1</span></li>
						<li><a class='page-numbers' href='https://hauteecoledejoaillerie.com/actualites/page/2/#listing'>2</a></li>
						<li><a class='page-numbers' href='https://hauteecoledejoaillerie.com/actualites/page/3/#listing'>3</a></li>
						<li><span class="page-numbers dots">&hellip;</span></li>
						<li><a class='page-numbers' href='https://hauteecoledejoaillerie.com/actualites/page/11/#listing'>11</a></li>
						<li><a class="next page-numbers" href="https://hauteecoledejoaillerie.com/actualites/page/2/#listing">&raquo;</a></li>
					</ul>
				</div>
			</div>
		</section> -->
<!--
		9ème Module
			<div class="list-members grid-4_sm-3_xs-1-equalHeight">

				<div class="col">
					<div class="list-members--item">
						<a href="https://femmesdutourisme.org/membres/sylvia-avalos/">
							<div class="list-members--item--picture">
								<img src="https://femmesdutourisme.org/wp-content/uploads/2018/03/Photo-Avalos-site-1.jpg" alt="" />
							</div>
							<h3 class="list-members--item--title">Sylvia Avalos</h3>
								<p class="list-members--item--job">Directrice</p>
							<div class="list-members--item--company">ANAPIA VOYAGES</div>
						</a>
					</div>
				</div>
			</div>

			<div class="col">
					<div class="list-members--item">
						<a href="https://femmesdutourisme.org/membres/sylvia-avalos/">
							<div class="list-members--item--picture">
								<img src="https://femmesdutourisme.org/wp-content/uploads/2018/03/Photo-Avalos-site-1.jpg" alt="" />
							</div>
							<h3 class="list-members--item--title">Sylvia Avalos</h3>
								<p class="list-members--item--job">Directrice</p>
							<div class="list-members--item--company">ANAPIA VOYAGES</div>
						</a>
					</div>
				</div>

				<div class="col">
					<div class="list-members--item">
						<a href="https://femmesdutourisme.org/membres/sylvia-avalos/">
							<div class="list-members--item--picture">
								<img src="https://femmesdutourisme.org/wp-content/uploads/2018/03/Photo-Avalos-site-1.jpg" alt="" />
							</div>
							<h3 class="list-members--item--title">Sylvia Avalos</h3>
								<p class="list-members--item--job">Directrice</p>
							<div class="list-members--item--company">ANAPIA VOYAGES</div>
						</a>
					</div>
				</div>

				<div class="col">
					<div class="list-members--item">
						<a href="https://femmesdutourisme.org/membres/sylvia-avalos/">
							<div class="list-members--item--picture">
								<img src="https://femmesdutourisme.org/wp-content/uploads/2018/03/Photo-Avalos-site-1.jpg" alt="" />
							</div>
							<h3 class="list-members--item--title">Sylvia Avalos</h3>
								<p class="list-members--item--job">Directrice</p>
							<div class="list-members--item--company">ANAPIA VOYAGES</div>
						</a>
					</div>
				</div>

				<div class="col">
					<div class="list-members--item">
						<a href="https://femmesdutourisme.org/membres/sylvia-avalos/">
							<div class="list-members--item--picture">
								<img src="https://femmesdutourisme.org/wp-content/uploads/2018/03/Photo-Avalos-site-1.jpg" alt="" />
							</div>
							<h3 class="list-members--item--title">Sylvia Avalos</h3>
								<p class="list-members--item--job">Directrice</p>
							<div class="list-members--item--company">ANAPIA VOYAGES</div>
						</a>
					</div>
				</div>

				<div class="col">
					<div class="list-members--item">
						<a href="https://femmesdutourisme.org/membres/sylvia-avalos/">
							<div class="list-members--item--picture">
								<img src="https://femmesdutourisme.org/wp-content/uploads/2018/03/Photo-Avalos-site-1.jpg" alt="" />
							</div>
							<h3 class="list-members--item--title">Sylvia Avalos</h3>
								<p class="list-members--item--job">Directrice</p>
							<div class="list-members--item--company">ANAPIA VOYAGES</div>
						</a>
					</div>
				</div> -->

		<!-- 10ème Module -->

			<!-- Module Footer -->
			<section class="page-content--section page-content--section__full-widthSm page-content--section__color-light">
			<div class="content-container content-container__sm center">
				<h2 class="title-section mbt">Voir aussi</h2>
				<ul class="list-related">
					<li class="footer-li">
						<div id="losange-90"></div>
						<a href="#">Un peu d’histoire</a>
					</li>
					<li class="footer-li">
						<div id="losange-90"></div>
						<a href="#">Les 4 départements</a>
					</li>
					<li class="footer-li">
						<div id="losange-90"></div>
						<a href="#">Le developpement à l’international</a>
					</li>
				</ul>
			</div>
			<!-- /.content-container -->
		</section>

		*/ ?>
	</section>
</div>
<?php get_footer(); ?>
