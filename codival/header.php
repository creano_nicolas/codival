<!--
  Credits

    Design & Development
      Creano
      https://creano.paris
-->
<!doctype html>
<html <?php language_attributes(); ?>>

<?php
use MBN\Menu;
$menu = new Menu('header-menu', null);

$home_url = get_home_url();
if (substr($home_url, -1) != '/'){
	$home_url .= '/';
}
?>

<head>
	<!-- charset -->
	<meta charset="UTF-8">
	<!-- wphead -->
	<?php wp_head(); ?>
	<!-- resp -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-1141066-48"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'UA-1141066-48');
	</script> -->
</head>

<body <?php body_class(); ?>>

	<!-- if !no-ajax -->
	<div class="page-loader">
		<div>
			<div></div>
		</div>
	</div>
	<?php /* Si nav fixed, html ici */ ?>
	<?php /* si nav non-fixe, html ici */ ?>
		<header class="header--main">
			<!-- desktop sitenav -->
			<nav id="header-nav" class="sitenav">

				<form class="sitenav-searchbar" method="GET" action="<?= $home_url; ?>" role="search">
					<div class="sitenav-searchbar--inner">
						<!-- <input type="text"> -->
						<input class="search-input" type="search" name="s" placeholder="<?php pll_e("Rechercher...")  ?>">
						<button>OK</button>
						<span class="sitenav-searchbar__close">X</span>
					</div>
				</form>

				<!-- LEVEL 1 sitenav -->
				<div class="sitenav--inner">
					<div class="sitenav--title">
						<a href="<?php echo home_url(); ?>">
							<img src="<?= get_template_directory_uri() ?>/svg/svgo/codival-horizontal-logo.svg" alt="Codival" />
						</a>
					</div>
					<?php // DESKTOP MENU
					include 'templates/desktop-menu.php'; ?>
					<div class="sitenav-lang">
						<ul class="polylang--nav">
							<?php pll_the_languages(array('display_names_as' =>'slug', 'hide_curent' => 0, 'hide_if_empty' => 0, 'hide_if_no_translation' => 0)); ?>
						</ul>
					</div>
					<div class="sitenav-search">
						<img id="search_icon" src="<?= get_template_directory_uri() ?>/svg/svgo/search.svg">
					</div>
				</div>

			</nav>
			<!-- /desktop sitenav -->

			<!-- mobile sitenav -->
			<nav id="header-nav-mobile" class="mobile-sitenav">
				<div class="mobile-sitenav--inner">

					<!-- Logo -->
					<div class="mobile-sitenav--item mobile-sitenav--title">
						<a class="mobile-sitenav--title--logo" href="<?php echo home_url(); ?>">
							<img src="<?= get_template_directory_uri() ?>/svg/svgo/codival-horizontal-logo.svg" alt="Codival" />
						</a>
					</div>

					<div class="mobile-sitenav--group">

						<!-- Burger -->
						<div class="mobile-sitenav--item">
							<button id="fwmb-trigger" class="fwmb-trigger">
								<div class="mobile-sitenav--burger">
									<div class="mobile-sitenav--burger--lines">
										<div class="mobile-sitenav--burger--line"></div>
										<div class="mobile-sitenav--burger--line"></div>
										<div class="mobile-sitenav--burger--line"></div>
									</div>
								</div>
							</button>

							<?php // MOBILE MENU
								include 'templates/mobile-menu.php'; ?>
						</div>
					</div>

				</div>
			</nav>
		</header>
		<div class="scroll-container"><?php /* container avec smooth scroll (translation en Y) */ ?>
			<div class="global-container">
