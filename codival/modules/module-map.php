<section class="module--wysiwyg scroll-reveal" reveal-offset="300">
	<!-- <div class="content-container content-container__l">
		<div class="module--wysiwyg__content"> -->

			<div id="map-ctnr">
				<?php /*<div class="wrapper-large">
					<div class="map-buttons-ctnr clearfix">
						<button id="allMarkers" class="map-button-activ"><span>Tous</span></button>
						<button id="markers1" class="map-button-activ"><span>AEL</span></button>
						<button id="markers2"><span>ALFA</span></button>
					</div>
				</div>*/ ?>
				<div class="wrapper-large wrapper-large--map">
					<div id="map"></div>
				</div>
			</div>

		<!-- </div>
	</div> -->
</section>

<?php //if (is_page_template('tpl-activite.php')): ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7dK1WqiyHRpbkUZjfglrLV74J0JufoCc&callback=initMap" async defer></script>
<script>
function initMap() {

	// Create a new StyledMapType object, passing it an array of styles,
	// and the name to be displayed on the map type control.
	var styledMapType = new google.maps.StyledMapType(
  [
    {elementType: 'geometry', stylers: [{color: '#ebe3cd'}]},
    {elementType: 'labels.text.fill', stylers: [{color: '#523735'}]},
    {elementType: 'labels.text.stroke', stylers: [{color: '#f5f1e6'}]},
    {
      featureType: 'administrative',
      elementType: 'geometry.stroke',
      stylers: [{color: '#c9b2a6'}]
    },
    {
      featureType: 'administrative.land_parcel',
      elementType: 'geometry.stroke',
      stylers: [{color: '#dcd2be'}]
    },
    {
      featureType: 'administrative.land_parcel',
      elementType: 'labels.text.fill',
      stylers: [{color: '#ae9e90'}]
    },
    {
      featureType: 'landscape.natural',
      elementType: 'geometry',
      stylers: [{color: '#f2efe9'}]
    },
    {
      featureType: 'poi',
      elementType: 'geometry',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'poi',
      elementType: 'labels.text.fill',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'poi.park',
      elementType: 'geometry.fill',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'poi.park',
      elementType: 'labels.text.fill',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [{color: '#f5f1e6'}]
    },
		{
			featureType: 'road',
			elementType: 'labels',
			stylers: [{'visibility': 'off'}]
    },
    {
      featureType: 'road.arterial',
      elementType: 'geometry',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'road.highway',
      elementType: 'geometry.stroke',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'road.highway.controlled_access',
      elementType: 'geometry',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'road.highway.controlled_access',
      elementType: 'geometry.stroke',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'road.local',
      elementType: 'labels.text.fill',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'transit.line',
      elementType: 'geometry',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'transit.line',
      elementType: 'labels.text.fill',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'transit.line',
      elementType: 'labels.text.stroke',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'transit.station',
      elementType: 'geometry',
      stylers: [{color: 'transparent'}]
    },
    {
      featureType: 'water',
      elementType: 'geometry.fill',
      stylers: [{color: '#b5d0d0'}]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.fill',
      stylers: [{color: '#414141'}]
    }
  ],
  {name: 'Styled Map'});


  var myCenter = new google.maps.LatLng(7.539989, -5.547080);
  var mapCanvas = document.getElementById("map");
  var mapOptions = {center: myCenter, zoom: 7, mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map']};
  var map = new google.maps.Map(mapCanvas, mapOptions);

  //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('styled_map', styledMapType);
  map.setMapTypeId('styled_map');

  var redIcon = "<?php echo get_template_directory_uri(); ?>/assets/img/pin-gmap.svg";
  var blueIcon = "<?php echo get_template_directory_uri(); ?>/assets/img/pin-gmap.svg";

	jQuery.getJSON( "<?php echo get_template_directory_uri(); ?>/markers.json", function( data ) {

	  var items = [];
	  var infowindows = [];
	  var markerGroupArray = [];

	  jQuery.each( data, function( key, val ) {


			var markerGroup = val.markergroup;
		  markerGroupArray.push( markerGroup );

		  if(markerGroup == 1){
		  	icon = blueIcon;
		  } else {
		  	icon = redIcon;
		  }

		  var mapMarker = new google.maps.LatLng(val.latitude, val.longitude);
		  var marker = new google.maps.Marker({position:mapMarker, icon:icon});
		  marker.setMap(map);
		  items.push( marker );

		  var infowindow = new google.maps.InfoWindow({
		  	content:'<h2>' + val.title + '</h2>' + val.descriptionhtml + '<a target="_blanck" href="' + val.hrefsite + '">' + val.hrefsite + '</a>'
		  });
		  infowindows.push( infowindow );

	  }); // Endforeach

	  jQuery.each(items, function( key, val ){
	  	google.maps.event.addListener(val, 'click', function(){

	  		jQuery.each(infowindows, function(key2, val2){
	  			val2.close(map,val);
	  			if(key == key2){
	  				val2.open(map,val);
	  			}
	  		});

		  });
	  });

	  jQuery('#markers1').click(function(){
	  	jQuery.each(items, function(key, val){

	  		jQuery.each(markerGroupArray, function(key2, val2){
	  			if(val2 == 2 && key == key2){
	  				val.setMap(null);
	  			}
	  			if(val2 == 1 && key == key2){
	  				val.setMap(map);
	  			}
	  		});

	  	});
	  })

	  jQuery('#markers2').click(function(){
	  	jQuery.each(items, function(key, val){

	  		jQuery.each(markerGroupArray, function(key2, val2){
	  			if(val2 == 1 && key == key2){
	  				val.setMap(null);
	  			}
	  			if(val2 == 2 && key == key2){
	  				val.setMap(map);
	  			}
	  		});

	  	});
	  })

	  jQuery('#allMarkers').click(function(){
	  	jQuery.each(items, function(key, val){
	  		val.setMap(map);
	  	});
	  });

	});
}
// initMap();
</script>
<?php //endif; ?>
