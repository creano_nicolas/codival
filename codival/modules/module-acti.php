<div class="acti">
	<?php
	$timeline = array(
		"stroke_color"    => "green",
		"out"             => "orange",
		"in"              => "green",
		"text_color"      => "green",
		"text"            => $module['title_tl'],
	);
	include(locate_template('modules/module-timeline.php'));
	?>
	<div class="acti--bg">
		<?php include(locate_template('svg/fade-logo.php')); ?>
	</div>
	<div class="acti--bcenter">
		<div class="acti--head">
			<div class="sqtitle scroll-reveal">
				<div class="sqtitle--in">
					<div class="decoangle decoangle__orange decoangle__tr decoangle__sm"></div>
					<div class="decoangle decoangle__orange decoangle__bl decoangle__md"></div>
					<div class="sqtitle--title"><?= $module['title']; ?></div>
				</div>
			</div>
			<div class="acti--desc">
				<?php echo apply_filters('the_content', $module['desc']); ?>
			</div>
		</div>
	</div>
	<div class="acti--sscont">
		<div class="basicss basicss__nooverflow">
			<?php if (count($module['slides']) > 1) : ?>
				<div class="basicss--nav">
					<div class="basicss--prev arrowcircle">
						<?php get_template_part('svg/arrow-circle-left'); ?>
					</div>
					<div class="basicss--next arrowcircle">
						<?php get_template_part('svg/arrow-circle-right'); ?>
					</div>
				</div>
			<?php endif ?>
			<div class="basicss--slider scroll-reveal">
				<?php foreach ($module['slides'] as $block) : ?>
					<div class="basicss--item">
						<div class="acticard">
							<div class="acticard--head">
								<div class="acticard--imgc">
									<?php
									$img                   = $block['image'];
									$img_alt               = get_post_meta($img, '_wp_attachment_image_alt', true);
									$attachement_image_src = wp_get_attachment_image_src($img, 'img_size');
									$img_srcset            = wp_get_attachment_image_srcset($img, 'img_size');
									?>
									<img class="img-cover" data-object-fit="cover" src="<?php echo $attachement_image_src[0]; ?>" srcset="<?php echo $img_srcset; ?>" alt="<?php echo $img_alt; ?>">
								</div>
								<div class="acticard--headline">
									<h2 class="acticard--title"><?= $block['title']; ?></h2>
								</div>
							</div>
							<div class="acticard--desc">
								<?php echo apply_filters('the_content', $block['desc']); ?>
							</div>
							<div class="acticard--ctac">
								<a href="<?= $block['link']; ?>" class="arcta arcta__cc000 arcta__arrow">
									<span class="arcta--text"><?= $block['link_text']; ?></span>
									<span class="arcta--arrow"><?php get_template_part('svg/button-arrow'); ?></span>
								</a>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
