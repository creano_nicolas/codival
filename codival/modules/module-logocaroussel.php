<section class="module module-logocaroussel scroll-reveal">
		<?php if(isset($module["title"])): ?>
			<div class="content-container">
				<h2 class="title-module"><?= $module["title"] ?></h2>
			</div>
		<?php endif; ?>
		<div class="content-container">
			<div class="logo-slider">
			<?php foreach ($module["logo"] as $logo) : ?>
				<div class="logo-slider__slide">
						<img src="<?= wp_get_attachment_image_src($logo, "medium")[0]; ?>" alt="" />
				</div>
			<?php endforeach; ?>
			</div>
			<button class="logo-slider__arrow logo-slider__arrow--prev"></button>
			<button class="logo-slider__arrow logo-slider__arrow--next"></button>
		</div>
</section>
