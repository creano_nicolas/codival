<section class="module scroll-reveal module-chiffres scroll-reveal" reveal-offset="300">
	<div class="content-container content-container__sm">
		<div class="module--wysiwyg__content">
				<div class="markup">
					<h3><?= $module["title"] ?></h3>
					<p><?= $module["intro"] ?></p>
				</div>
		</div>
	</div>
	<div class="content-container">
		<div class="chiffres-body">
			<?php foreach ($module['chiffres'] as $block) : ?>
			<div class="chiffre">
				<h4 class="chiffre-value"><?= $block["chiffre"] ?></h4>
				<p class="chiffre-desc"><?= $block["desc"] ?></p>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
