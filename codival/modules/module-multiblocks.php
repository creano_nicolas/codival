<section class="module module--multiblocks scroll-reveal" reveal-offset="300">

	<?php if (!empty($module["text_title"])) : ?>
		<div class="content-container">
			<h2 class="title-module"><?= $module["text_title"] ?></h2>
		</div>
	<?php endif; ?>
	<div class="content-container">
		<div class="grid-4_md-3_sm-2_xs-1">
			<?php foreach ($module['blocks'] as $block) : ?>
				<div class="col">
					<img src="<?= get_template_directory_uri() ?>/assets/img/title-block.svg" />
					<h4 class="title-smallblock"><?= $block["title"] ?></h4>
					<p class="txt-smaller"><?= $block["text"] ?></p>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
