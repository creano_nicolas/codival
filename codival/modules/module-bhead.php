<div class="bhead">
	<div class="bhead--deco">
		<div class="decoangle decoangle__green decoangle__tr decoangle__lg"></div>
		<div class="decoangle decoangle__orange decoangle__bl decoangle__bg"></div>
		<?php /* <div class="bhead--deco-circ"><?php get_template_part('svg/big-circle-header'); ?></div>*/ ?>
	</div>
	<div class="bhead--bgslide">
		<?php foreach ($module['slides'] as $block) : ?>
			<div class="bhead--bgitem">
				<?php if ($block['image']) : ?>
					<?php
					$img                   = $block['image'];
					$img_alt               = get_post_meta($img, '_wp_attachment_image_alt', true);
					$attachement_image_src = wp_get_attachment_image_src($img, 'img_size');
					$img_srcset            = wp_get_attachment_image_srcset($img, 'img_size');
					?>
					<img class="img-cover" data-object-fit="cover" src="<?php echo $attachement_image_src[0]; ?>" srcset="<?php echo $img_srcset; ?>" alt="<?php echo $img_alt; ?>">
				<?php endif ?>
				<?php if ($block['video']) : ?>
					<?php
					$url = wp_get_attachment_url($block['video']);
					?>
					<video class="video-cover" loop="true" preload="true" muted="muted" playsinline autoplay src="<?= $url; ?>" type="video/mp4"></video>
				<?php endif ?>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="bhead--slides">
		<?php foreach ($module['slides'] as $block) : ?>
			<div class="bhead--slide">
				<div class="bhead--title"><?= $block['title']; ?></div>
				<div class="bhead--cta">
					<a href="<?= $block['link']; ?>" class="arcta arcta__ccfff arcta__arrow">
						<span class="arcta--text"><?= $block['link_text']; ?></span>
						<span class="arcta--arrow"><?php get_template_part('svg/button-arrow'); ?></span>
					</a>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<?php if (count($module['slides']) > 1) : ?>
		<div class="bhead--nav">
			<div class="bhead--prev arrowcircle">
				<?php get_template_part('svg/arrow-circle-left'); ?>
			</div>
			<div class="bhead--next arrowcircle">
				<?php get_template_part('svg/arrow-circle-right'); ?>
			</div>
		</div>
	<?php endif ?>
</div>
