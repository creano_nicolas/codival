<div>
<section class="module module-logotabs scroll-reveal" reveal-offset="300">
	<div class="content-container logo-tabsfilter">
	<div class="gallery-filters">
		<span class="gallery-filters-title"><?= $module["type"] ?>  :</span>
		<ul>
		<?php foreach ($module["tabs"] as $tab): ?>
			<li><span><?= $tab["tabname"] ?></span></li>
		<?php endforeach; ?>
		</ul>
	</div>
	</div>
	<div class="content-container content-container__sm ">
		<div class="logotabs-wrapper">
			<?php foreach ($module["tabs"] as $tab): ?>
				<div class="logotabs-slide">
					<?php foreach ($tab["logos"] as $logo) : ?>
						<a href="<?= $logo["url"] ?>" target="_blank" class="no-link">
							<img src="<?= wp_get_attachment_image_src($logo["img"], "large")[0]; ?>">
						</a>
					<?php endforeach; ?>
					<?php if($tab['text']): ?>
						<div class="markup"><?php echo $tab['text'] ?></div>
					<?php endif ?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	</section>
</div>
