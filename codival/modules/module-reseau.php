<section class="module">
	<div class="content-container">
		<nav class="section-nav">
			<h2 class="title-section"><?= $module["title"] ?></h2>
			<?php if($module["link_text"]): ?>
			<div class="section-links">
				<a href="<?= $module["link_url"] ?>"><?= $module["link_text"] ?><i></i></a>
			</div>
			<?php endif; ?>
		</nav>
		<div class="adresse-listing">
			<?php foreach($module["adresses"] as $adresse): ?>
			<div class="adresse">
				<h4><?= $adresse["name"] ?></h4>
				<p><?= $adresse["adresse"] ?></p>
				<span><?= $adresse["phone"] ?></span>
			</div>
			<?php endforeach; ?>
	</div>
</section>
