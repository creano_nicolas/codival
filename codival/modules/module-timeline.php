<div class="timeline">
  <div class="timeline--stroke">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2 3000"><path fill="none" class="stroke<?= $timeline['stroke_color']; ?>" stroke-dasharray="5" d="M1 0v3000"/></svg>
  </div>
  <div class="timeline--headline">
    <div class="timeline--dot">
      <?php
        $bullet = array(
          "out" => $timeline['out'],
          "in"  => $timeline['in'],
        );
        include(locate_template('modules/module-bullet-point.php'));
      ?>
    </div>
    <div class="timeline--title cc<?= $timeline['text_color']; ?>"><?= $timeline['text']; ?></div>
  </div>
</div>