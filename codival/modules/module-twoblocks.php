<section class="module module--twoblocks scroll-reveal" reveal-offset="300">
	<div class="content-container">
		<?php if (!empty($module["text_title"])) : ?>
			<h2 class="title-section"><?= $module["text_title"] ?></h2>
		<?php endif; ?>
		<div class="grid-2_xs-1">
			<div class="col mbt">
				<p class="txt-bigger"><?= $module["text_1"] ?></p>
			</div>
			<div class="col mbt">
				<p class="mbs"><?= $module["text_2"] ?></p>
				<?php if (!empty($module["text_cta"])) : ?>
					<a href="<?= get_permalink($module['link_cta'][0]['id']); ?>" class="btn btn__invert"><?= $module["text_cta"] ?><i class=" btn--arrow"></i></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
