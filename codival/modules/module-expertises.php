<section class="module scroll-reveal expertise module-green scroll-reveal" reveal-offset="300">
	<div class="content-container">
		<div class="body-expertises">
			<?php foreach ($module['blocks'] as $block) : ?>
			<div class="expertises-col">
				<?php if(isset(wp_get_attachment_image_src($block["picto"], "small")[0])): ?>
				<img class="picto-expertises" src="<?= wp_get_attachment_image_src($block["picto"], "small")[0]; ?>">
				<?php endif; ?>
				<h4 class="title-smallblock-bold"><?= $block["title"] ?></h4>
				<p class="txt-smaller"><?= $block["text"] ?></p>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
