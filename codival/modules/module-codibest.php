<div class="codibest">
	<?php
	$timeline = array(
		"stroke_color"    => "white",
		"out"             => "orange",
		"in"              => "white",
		"text_color"      => "white",
		"text"            => $module['title_tl'],
	);
	include(locate_template('modules/module-timeline.php'));
	?>
	<div class="codibest--bcenter">
		<div class="sqtitle scroll-reveal">
			<div class="sqtitle--in">
				<div class="decoangle decoangle__orange decoangle__tr decoangle__sm"></div>
				<div class="decoangle decoangle__orange decoangle__bl decoangle__md"></div>
				<div class="sqtitle--title"><?= $module['title']; ?></div>
			</div>
		</div>
		<div class="codibest--desc">
			<?php echo apply_filters('the_content', $module['desc']); ?>
		</div>

		<a href="<?= $module['link']; ?>" class="arcta arcta__ccfff arcta__arrow">
			<span class="arcta--text"><?= $module['link_text']; ?></span>
			<span class="arcta--arrow"><?php get_template_part('svg/button-arrow'); ?></span>
		</a>
	</div>
</div>
