<section class="module module--socialshare scroll-reveal" reveal-offset="300">
	<div class="content-container">
		<p><?= pll_e("Partager sur") ?> :</p>
		<div>
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink(); ?>" target="_blank"><?php get_template_part( 'assets/svg/facebook' ); ?></a>
			<a href="http://www.twitter.com/share?url=<?= get_the_permalink(); ?>" target="_blank"><?php get_template_part( 'assets/svg/twitter' ); ?></a>
			<a href="https://www.linkedin.com/sharing/share-offsite/?url=<?= get_the_permalink(); ?>" target="_blank"><?php get_template_part( 'assets/svg/linkedin' ); ?></a>
		</div>
	</div>
</section>
