<section class="module scroll-reveal" reveal-offset="300">
	<div class="content-container">
		<?php if(isset($module["title"])): ?>
			<h2 class="title-module"><?= $module["title"] ?></h2>
		<?php endif; ?>
		<ul class="membersbook">
			<?php foreach ($module["members"] as $member): ?>
				<li class="membersbook--member <?php if($member['management']) echo "membersbook--member--management" ?> scroll-reveal" reveal-offset="300">
						<img class="membersbook--member--photo" src="<?= $member["photo"]?>" alt="<?= $member["name"] ?>">
						<h3 class="membersbook--member--name"><?=  $member["name"] ?></h3>
						<p class="membersbook--member--job"><?= $member['job']; ?></p>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</section>
