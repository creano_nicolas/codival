<div class="codires">
  <?php
  	$timeline = array(
      "stroke_color" 		=> "green",
      "out"     				=> "orange",
      "in"      				=> "green",
      "text_color" 			=> "green",
      "text"    				=> $module['title_tl'],
  	);
  	include(locate_template('modules/module-timeline.php'));
  ?>
	<div class="codires--bg">
		<?php include(locate_template('svg/africa-graphism.php')); ?>
	</div>
  <div class="codires--bcenter">
  	<div class="codires--head">
			<div class="sqtitle scroll-reveal">
				<div class="sqtitle--in">
					<div class="decoangle decoangle__orange decoangle__tr decoangle__sm"></div>
					<div class="decoangle decoangle__orange decoangle__bl decoangle__md"></div>
					<div class="sqtitle--title"><?= $module['title']; ?></div>
				</div>
			</div>
			<div class="codires--desc">
				<?php echo apply_filters('the_content', $module['desc']); ?>
			</div>
		</div>
	</div>
	<div class="codires--sscont">
		<div class="basicss basicss__nooverflow">
      <?php if (count($module['slides']) > 1): ?>
				<div class="basicss--nav">
					<div class="basicss--prev arrowcircle">
						<?php get_template_part('svg/arrow-circle-left'); ?>
					</div>
					<div class="basicss--next arrowcircle">
						<?php get_template_part('svg/arrow-circle-right'); ?>
					</div>
				</div>
      <?php endif ?>
			<div class="basicss--slider scroll-reveal">
				<?php foreach ($module['slides'] as $block) : ?>
					<div class="basicss--item">
						<div class="rescard">
							<div class="rescard--logoc">
                <div class="rescard--logoborder">
                  <?php get_template_part('svg/rescard-border'); ?>
                </div>
                <?php
                  $img                   = $block['image'];
                  $img_alt               = get_post_meta($img, '_wp_attachment_image_alt', true);
                  $attachement_image_src = wp_get_attachment_image_src($img, 'img_size');
                  $img_srcset            = wp_get_attachment_image_srcset($img, 'img_size');
                ?>
                <img
                  src="<?php echo $attachement_image_src[0]; ?>"
                  srcset="<?php echo $img_srcset; ?>"
                  alt="<?php echo $img_alt; ?>">
							</div>
							<div class="rescard--infos">
								<div class="rescard--title"><?= $block['title']; ?></div>
								<div class="rescard--desc">
									<?php echo apply_filters('the_content', $block['desc1']); ?>
								</div>
								<div class="rescard--contact">
									<?php echo apply_filters('the_content', $block['desc2']); ?>
								</div>
							</div>
						</div>
					</div>
        <?php endforeach; ?>
			</div>
		</div>
	</div>
</div>