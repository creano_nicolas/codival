<?php

	$similars = array();

	$current_page_url = trim(home_url(add_query_arg(array(),$wp->request)), '/')."/";
	$menuitems = mbn_get_menu_items("header-menu");

	$currentmenuitem = null;
	foreach($menuitems as $menuitem){
		if($current_page_url == $menuitem->url){
			$currentmenuitem = $menuitem;
			break;
		}
	}

	if($currentmenuitem != null){
		$parentitem = $currentmenuitem;
		foreach($menuitems as $menuitem){
			if($currentmenuitem->menu_item_parent == $menuitem->ID){
				$parentitem = $menuitem;
				break;
			}
		}

		foreach($menuitems as $menuitem){
			if(($menuitem->menu_item_parent == $parentitem->ID && $currentmenuitem->ID != $menuitem->ID) || ($menuitem->ID == $parentitem->ID && $currentmenuitem->ID != $menuitem->ID)){
				$similars[] = array(
					"name" => $menuitem->title,
					"url" => $menuitem->url,
				);
			}
		}
	}

	if(get_post_type() != "page" && !isset($currentmenuitem)){
		$params = [
			'post_type' => get_post_type(),
			'posts_per_page' => 0,
			'orderby'   => 'date',
			'order' => 'DESC',
			'post__not_in' => array(get_the_ID()),
		];
		$loop = new WP_Query( $params );
		while ( $loop->have_posts() ){
			$loop->the_post();
			$similars[] = array(
				"name" => get_the_title(),
				"url" => get_the_permalink(),
			);
		}
	}

?>
<?php if(count($similars) > 0): ?>
<section class="module module-grey">

	<div class="content-container">
		<h2 class="title-module"><?php pll_e("Voir aussi") ?></h2>
	</div>

	<div class="content-container content-container__sm">

		<ul class="listlosange">
			<?php foreach ( $similars as $similar) : ?>
				<li>
					<a href="<?= $similar["url"] ?>"><?= $similar["name"] ?></a>
				</li>
			<?php endforeach; ?>
		</ul>

	</div>
</section>
<?php endif; ?>
