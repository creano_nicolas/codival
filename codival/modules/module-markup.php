<section class="module <?php if($module['color'] == "green"): ?>module-green <?php endif; ?>module--wysiwyg scroll-reveal" reveal-offset="300">
	<div class="content-container content-container__sm">
		<div class="module--wysiwyg__content">
				<div class="markup"><?php echo $module['text'] ?></div>
		</div>
	</div>
</section>
