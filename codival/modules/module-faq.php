<section class="module module-faq scroll-reveal">
		<div class="content-container">
			<h2 class="title-module"><?= $module["title"] ?></h2>
			<div class="content-container__right">
			<ul class="faq">
				<?php foreach($module["questions"] as $question): ?>
					<li>
						<h4><?= $question["question"] ?></h4>
						<p><?= $question["answer"] ?></p>
					</li>
				<?php endforeach; ?>
			</ul>
			<div>
		</div>
</section>
