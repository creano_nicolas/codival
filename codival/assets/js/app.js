// libs
/*import TweenMax from "gsap/TweenMax"
import jQuery from "jquery"*/
import sniffer from "./utils/sniffer.js"
window.$ = window.jQuery = jQuery
window.startOptions = {
	ajax: true,
	smoothscroll: true
}

// base
//import initSite from './base/initSite'// when site is init (not to be confused with when page is init)
import preloader from './base/preloader.js'

$(function() {
    //initSite()
	preloader()

    if (sniffer.isIE) {
        console.log('isIE')
        $('html').addClass('js-isIE')
    }
    if (sniffer.isEdge) {
        console.log('isEdge')
        $('html').addClass('js-isEdge')
    }
});

// function goBack() {
//     window.history.back();
// }
