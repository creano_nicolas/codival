import { _debounce, _throttle } from "../utils/utils.js";
function scrollReveal() {
	$("body").on("reveal", ".scroll-reveal", scrollRevealHandler);
	var seek = function() {
		var scrollTop = $(window).scrollTop();
		var windowHeight = $(window).height();

		var $el = $(".scroll-reveal");
		if ($el != null) {
			$el.each(function() {
				var $self = $(this);
				if ($self.hasClass("is-revealed")) {
					return;
				}

				var offset = $self.attr("reveal-offset")
					? parseInt($self.attr("reveal-offset"))
					: 0;

				var offsetTop = $self.offset().top;
				if (scrollTop + windowHeight - offset >= offsetTop) {
					$self.trigger("reveal");
				}
			});
		}
	};

	$(window).on(
		"scroll",
		_throttle(function() {
			requestAnimationFrame(seek);
		}, 1000 / 20)
	); // on scroll, 50 times per second, launch the function (_throttle)
	$(window).on(
		"scroll",
		_debounce(function() {
			requestAnimationFrame(seek);
		}, 250)
	); // at the end of a scroll, launch the function after 250ms (_debounce)
}

var scrollRevealHandler = function() {
	if (!window.canReveal) return;

	var $el = $(this);
	if ($el.hasClass("is-revealed")) return;

	$el.addClass("is-revealed");

	if ($el.hasClass("sqtitle")) {

		var tl = new TimelineLite();
		tl.fromTo( $el.find('.decoangle__tr'), 1, { alpha: 0, y: -30, x:30 },  { alpha: 1, y: 0, x: 0, ease: Power3.easeOut }, 0 );
		tl.fromTo( $el.find('.decoangle__tl'), 1, { alpha: 0, y: -30, x:-30 }, { alpha: 1, y: 0, x: 0, ease: Power3.easeOut }, 0 );
		tl.fromTo( $el.find('.decoangle__br'), 1, { alpha: 0, y: 30, x:30 },   { alpha: 1, y: 0, x: 0, ease: Power3.easeOut }, 0 );
		tl.fromTo( $el.find('.decoangle__bl'), 1, { alpha: 0, y: 30, x:-30 },  { alpha: 1, y: 0, x: 0, ease: Power3.easeOut }, 0 );

		tl.fromTo( $el.find('.sqtitle--title'), 1, { alpha: 0, y: 20 },  { alpha: 1, y: 0, ease: Power3.easeOut }, 0 );

	} else if ($el.hasClass('basicss--slider')) {

		var tl = new TimelineLite();
		tl.staggerFromTo( $el.find('.basicss--item'), 1, { alpha: 0, y: 50}, { alpha: 1, y: 0, ease: Power3.easeOut }, 0.1, 0 );

	} else {
		var tl = new TimelineLite();
		tl.fromTo(
			$el,
			1,
			{
				alpha: 0,
				y: 50
			},
			{
				alpha: 1,
				y: 0,
				ease: Power3.easeOut
			},
			0
		);
	}
};

export default scrollReveal;
