// _____________________________ startsWith polyfill
// _______________________________________________________
if (!String.prototype.startsWith) {
  Object.defineProperty(String.prototype, 'startsWith', {
    value: function(search, pos) {
      pos = !pos || pos < 0 ? 0 : +pos;
      return this.substring(pos, pos + search.length) === search;
    }
  });
}

// _____________________________ real tap
// _______________________________________________________
  function _onRealTap($el, callback) {
    var tapTime;
    var moving = false;
    var move = 0;
    var moveOrigin = 0;
    $el.on('touchstart mousedown touchend mouseup touchmove mousemove', function (e) {
      var $self = $(this);

      if (e.type === 'touchstart' || e.type === 'mousedown') {
        moving = true;
        tapTime = Date.now();
        moveOrigin = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
        move = 0;
      }

      if (e.type === 'touchend' || e.type === 'mouseup') {
        moving = false;
        var endTapTime = Date.now();

        if (endTapTime < tapTime + 200 && Math.abs(move) < 10) {
          if (callback) {
            callback($self);
          }
        }
      }

      if (e.type === 'touchmove' || e.type === 'mousemove') {
        if (moving) {
          var thisX = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
          move = thisX - moveOrigin;
        }
      }
    });
  };

// _____________________________ trimSlash
// _______________________________________________________
String.prototype.trimSlash = function () {
  return this.replace(/^\/+|\/+$/gm, '')
}

// _____________________________ trimHttp
// _______________________________________________________
String.prototype.trimHttp = function () {
  let text = this.replace('http://', '')
  text = text.replace('https://', '')
  return text
}

// _____________________________ addEndSlash
// _______________________________________________________
String.prototype.addEndSlash = function () {
  if (this.indexOf('?s=') != -1) {
    return this
  }
  let string = "".concat(this, "/")
  return string.replace(/\/\/+$/gm, '/')
}

// _____________________________ getTranslateY
// _______________________________________________________
function _getTranslateY(element) {
  var style = window.getComputedStyle(element.get(0));
  var matrix = style.getPropertyValue('-webkit-transform') || style.getPropertyValue('-moz-transform') || style.getPropertyValue('-ms-transform') || style.getPropertyValue('-o-transform') || style.getPropertyValue('transform');

  if (matrix === 'none') {
    matrix = 'matrix(0,0,0,0,0)';
  }

  var values = matrix.match(/([-+]?[\d\.]+)/g);
  return values[14] || values[5] || 0;
}

// _____________________________ getTranslateX
// _______________________________________________________
function _getTranslateX(element) {
  var style = window.getComputedStyle(element.get(0));
  var matrix = style.getPropertyValue('-webkit-transform') || style.getPropertyValue('-moz-transform') || style.getPropertyValue('-ms-transform') || style.getPropertyValue('-o-transform') || style.getPropertyValue('transform');

  if (matrix === 'none') {
    matrix = 'matrix(0,0,0,0,0)';
  }

  var values = matrix.match(/([-+]?[\d\.]+)/g);
  return values[14] || values[4] || 0;
}

// _____________________________ closest element from array
// _______________________________________________________
function _closest(x, arr) {
  let indexArr = arr.map(function (k) {
    return Math.abs(k - x);
  });
  let min = Math.min.apply(Math, indexArr);
  return arr[indexArr.indexOf(min)];
};

// _____________________________ stillExists
// _______________________________________________________
function _stillExists($elem){
    return $elem.closest("body").length > 0;
}

// _____________________________ throttle
// _______________________________________________________
function _throttle(callback, delay) {
  let last
  let timer
  return function () {
    let context = this
    let now = +new Date()
    let args = arguments
    if (last && now < last + delay) {
      clearTimeout(timer)
      timer = setTimeout(() => {
        last = now
        callback.apply(context, args)
      }, delay)
    } else {
      last = now
      callback.apply(context, args)
    }
  }
}

// _____________________________ debounce
// _______________________________________________________
function _debounce(func, wait, immediate) {
  let timeout
  return function() {
    let context = this
    let args = arguments
    let later = () => {
      timeout = null
      if (!immediate) func.apply(context, args)
    }
    let callNow = immediate && !timeout
    clearTimeout(timeout)
    timeout = setTimeout(later, wait)
    if (callNow) func.apply(context, args)
  }
}

export {_closest, _throttle, _debounce, _stillExists, _getTranslateY, _getTranslateX, _onRealTap}
