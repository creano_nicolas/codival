function search(){
	$(".sitenav-search").click(function(){
		$(".sitenav-searchbar").fadeIn(200);
    $(".search-input").focus();// focus on input to prevent a click
	});
	$(".sitenav-searchbar__close").click(function(){
		$(".sitenav-searchbar").fadeOut(200);
    $(".search-input").blur();// lose focus on input because it's not active anymore
	});
}

export default search;