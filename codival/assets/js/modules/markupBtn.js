function arcta(){

	$(".markup .arcta.custom-format").each(function(){
		let p = $(this).parent();
		let btn = `
			<a href="${ $(this).attr('href') }" class="arcta arcta__arrow custom-format">
				<span class="arcta--text">${ $(this).text() }</span>
				<span class="arcta--arrow"><svg xmlns="http://www.w3.org/2000/svg" width="42px" height="16px" viewBox="0 0 42 16"><path d="M33.1.4l-1.2 1.5 5.7 4.7H0v2h38.1l-5.9 5.8 1.4 1.4L42 7.6z"></path></svg></span>
			</a>
		`;
		p.html(btn);
	});

}

function markupBtn(){
	arcta();
	TweenLite.to(".markup .custom-format", .3, {
		autoAlpha: 1
	});
}

export default markupBtn;
