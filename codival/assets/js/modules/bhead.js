function bhead(){

  $('.bhead').each(function(){

    const $module = $(this)
    const $navPrev = $module.find('.bhead--prev')
    const $navNext = $module.find('.bhead--next')
    const $bgs = $module.find('.bhead--bgslide div')
    const $items = $module.find('.bhead--slide')
    const ssLength = $items.length
    let currentId = 0

    function travel(){
      currentId += $(this).is('.bhead--next') ? 1 : -1
      if (currentId < 0) currentId = ssLength-1
      if (currentId >= ssLength) currentId = 0
      updateContent()
    }

    function updateContent(){
      $bgs.removeClass('js-active')
      $bgs.eq(currentId).addClass('js-active')
      $items.removeClass('js-active')
      $items.eq(currentId).addClass('js-active')
    }

    function eventsOn(){
      $navPrev.on('click', travel)
      $navNext.on('click', travel)
      $('html').on('pageTransiLeave', eventsOff)// quand on quitte la page, on supprime tous les events = on ne bouffe plus de perf pour un élément inexistant
    }
    function eventsOff(){
      // quand on quitte la page, on supprime tous les events = on ne bouffe plus de perf pour un élément inexistant
      $navPrev.off('click', travel)
      $navNext.off('click', travel)
      $('html').off('pageTransiLeave', eventsOff)
    }

    eventsOn()// on lance tous les events
    updateContent()

  })

}
export default bhead