function logoslider() {
	// console.log("test");
	//return;
	$(".module-logocaroussel").each(function() {
		var $el = $(this).find(".logo-slider");
		// slides
		var $slides = $el.children("div");
		// if multiple slide
		if ($slides.length > 0) {
			$el.slick({
				dots: false,
				infinite: true,
				slidesToShow: 5,
				autoplay: true,
				autoplaySpeed: 2000,
				prevArrow: $(this).find(".logo-slider__arrow--prev"),
				nextArrow: $(this).find(".logo-slider__arrow--next"),
				responsive: [
					{
						breakpoint: 600,
						settings: {
							slidesToShow: 3
						}
					},
				]
			});
		}
		setTimeout(function() {
			$(window).trigger("resize");
		}, 1000);
	});
}

export default logoslider;
