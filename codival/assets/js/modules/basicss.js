function basicss(){
  $(".basicss").each(function() {
    const $module = $(this)
    const $ss = $module.find('.basicss--slider')
    const $items = $module.find('.basicss--item')
    const itemsLength = $items.length

    const $prev = $module.find('.basicss--prev')
    const $next = $module.find('.basicss--next')

    if (itemsLength) {
      $ss.slick({
        dots: false,
        infinite: false,
        variableWidth: true,
        centerMode: false,

				autoplay: true,
				autoplaySpeed: 4000,

        //swipe: false,
        prevArrow: $prev,
        nextArrow: $next,
      })
    }
    setTimeout(function() {
      $(window).trigger("resize")
    }, 1000)
  })
}
export default basicss
