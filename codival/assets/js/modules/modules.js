import ctslider from "./ct-slider.js";
import {fwmbMenu, fwmbMenuClose} from "./fwmbMenu.js";
import logoslider from "./logoslider.js";
import heroslider from "./hero-slider.js";
import search from "./search.js";
import drawer from "./drawer.js";

import logotabs from "./logotabs.js";
import bhead from "./bhead.js"
import basicss from "./basicss.js"
import {footgreen, footselect, footCreditLink} from "./footer.js"
import markupBtn from "./markupBtn.js";



//GLOBAL AU SITE GENRE LE BURGER
function siteModules() {
	fwmbMenu();
	search();
}

//PAGE SPECIFIQUE
function pageModules() {
	heroslider();
	ctslider();
	logoslider();
	fwmbMenuClose();
	drawer();
	markupBtn();

  bhead()
  basicss()
  footgreen()
	logotabs();
  footselect()
  footCreditLink();
  objectFitVideos(document.querySelectorAll('.video-cover'))// objectFitVideos = JS library
}

export { siteModules, pageModules };
