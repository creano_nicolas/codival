function ctslider() {

	$(".ct-slider").each(function() {
		var $el = $(this);

		// slides
		var $slides = $el.children("div");
		// if multiple slide
		if ($slides.length > 0) {
			$el.slick({
				dots: true,
				infinite: true,
				prevArrow: '<button class="slick-prev"></button>',
				nextArrow: '<button class="slick-next"></button>',
				responsive: [
					{
						breakpoint: 728,
						settings: {
							// speed: 700,
						}
					}
				]
			});

			// if slider in header
			$el
				.parent(".page-content--header")
				.addClass("page-content--header__has-slider");
		}
		setTimeout(function() {
			$(window).trigger("resize");
		}, 1000);
	});
}

export default ctslider;
