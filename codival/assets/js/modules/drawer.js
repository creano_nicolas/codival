function drawer_create({ target, hideAfter, button, margin = false, startOpen = false, matchMedia="(min-width: 0px)" }) {
  let el = typeof target === "string" ? $(target) : target;
  if(el.length == 0)
    return;
  let btn = typeof button === "string" ? $(button) : button;
  let firstchild = el.children().eq(hideAfter - 1);
  let lastchild = el.children().last();
  let hiddenH = 0;
  let revealH = 0;
  let open = startOpen;
  function caculateH() {
    hiddenH =
      firstchild.offset().top +
      firstchild.outerHeight(margin) -
      el.offset().top;
    revealH =
      lastchild.offset().top + lastchild.outerHeight(margin) - el.offset().top;
    if(window.matchMedia(matchMedia).matches && !open){
      open = false;
      el.css("max-height", `${hiddenH}px`);
      btn.removeClass("drawer-btn-open");
    }else{
      open = true;
      el.css("max-height",revealH);
      btn.addClass("drawer-btn-open");
    }
  }

  caculateH();
  $(window).resize(() => setTimeout(caculateH,500) );
  btn.click(() => {
    if(window.matchMedia(matchMedia).matches){
      open = !open;
      caculateH();
    }
  });
}

function drawer() {
	$(".faq li").each(function(){
		drawer_create({
			target: $(this),
			hideAfter: 1,
			button: $(this).find("h4"),
		});
	});
}

export default drawer;
