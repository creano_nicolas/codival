// put height on <a> links so we have the list at the same position
function footgreen(){
  const $foot = $('.footgreen')
  const $items = $foot.find('.menu-item-has-children > a')
  let maxHeight = 0
  function calcHeight(){
    maxHeight = 0
    $items.css('height', '')
    $items.each(function(){
      let thisH = $(this).outerHeight()
      maxHeight = thisH > maxHeight ? thisH : maxHeight
    })
    $items.css('height', maxHeight)
  }

  function eventsOn(){
    $(window).on('resize', calcHeight)
    $('html').on('pageTransiLeave', eventsOff)
  }
  function eventsOff(){
    $(window).off('resize', calcHeight)
    $('html').off('pageTransiLeave', eventsOff)
  }

  eventsOn()
  calcHeight()
}

// <select> to get good number
function footselect(){
  const $select = $('.footwhite--spec-select')
  const $result = $('.footwhite--spec-result')

  function update(){
    const val = $select.val()
    $result.html(val)
  }

  function eventsOn(){
    $select.on('change', update)
    $('html').on('pageTransiLeave', eventsOff)
  }
  function eventsOff(){
    $select.off('change', update)
    $('html').off('pageTransiLeave', eventsOff)
  }

  eventsOn()
  update()
}

function footCreditLink(){

  let $link = $('.footsub').find('.no-link a');
  $link.addClass('no-link');

}

export {footgreen, footselect, footCreditLink}
