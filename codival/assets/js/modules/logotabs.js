function logotabs() {
	// console.log("test");
	//return;
	$(".module-logotabs").each(function() {
		let slider = $(this).find(".logotabs-wrapper");
		slider.slick({
			arrows: false,
			swipe: false,
			touchMove : false,
			fade: true,
		  adaptiveHeight: true,
			responsive: [
				{
					breakpoint: 720,
					settings: {
						dots: true
					}
				}
			]
		});

		let tab = $(this).find(".gallery-filters ul li");
		tab.first().addClass("is-active");
		tab.click(function(){
			slider.slick("slickGoTo",$(this).index());
		});

		slider.on('afterChange', function(event, slick, currentSlide, nextSlide){
			var index = slider.slick("slickCurrentSlide");
			tab.removeClass("is-active");
			tab.eq(index).addClass("is-active");
		});
	});

	setTimeout(function() {
		$(window).trigger("resize");
	}, 1000);
}

export default logotabs;
