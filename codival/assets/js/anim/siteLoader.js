function siteLoaderExit(){
	//$(".page-loader").fadeOut();
	let tl = new TimelineMax();
	tl.to(".page-loader div",.3,{autoAlpha: 0, delay: 1});
	tl.to(".page-loader",.5,{autoAlpha: 0, delay: -0.1});
}

function siteLoaderUpdate(current, max){

}

export {siteLoaderExit,siteLoaderUpdate};
