// utils
import sniffer from "../utils/sniffer.js";

// scroll
import scroll from "../scroll/scroll.js";

function pageTransiLeave() {
	$('html').trigger('pageTransiLeave')// Passer l'info qu'on cache la page actuel
	// disable scroll
	//scroll.disable()

	// prevent to show next page before anim has finished
	window.canChangePage = false;
	setTimeout(function() {
		window.canChangePage = true;
	}, 500);

	// timeline
	let tl = new TimelineLite();

	// animation
	tl.to($(".global-container"), 0.5, { alpha: 0 }, 0);
	setTimeout(function() {
		$(window).scrollTop(0);
	}, 500);
}

export default pageTransiLeave;
