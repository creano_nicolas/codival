// utils
import sniffer from "../utils/sniffer.js";

// scroll reveal
import reveal from "../scroll/scrollReveal.js";

//import matchHeight from 'jquery-match-height'
import updateNavActive from "../base/updateNavActive.js";
import slugUpdate from "../base/slugUpdate.js";
import { pageModules } from "../modules/modules.js";
import objectFitPolyfill from "../utils/objectFitPolyfill.js";

// scroll ()
import scroll from "../scroll/scroll.js";

// when page is loaded
function initPage() {
	// trigger reveal (always after virtual scroll)
	window.canReveal = true;
	$(window).trigger("scroll");

	// put .js-current to every <a href="CURRENT-URL">
	updateNavActive();

	// update slug
	slugUpdate();

	// put .js-current to active lang
	let currentLang = $("html").attr("lang");
	$("[hreflang]").each(function() {
		let $self = $(this);
		let selfLang = $self.attr("hreflang");
		if (currentLang === selfLang) {
			$self.addClass("js-current");
		}
	});

	//INCLUDE MODULE
	pageModules();
	objectFitPolyfill();

}

export default initPage;
