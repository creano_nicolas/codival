import initPage from "./initPage.js";
import pageTransiEnter from "../anim/pageTransiEnter.js";
import pageTransiLeave from "../anim/pageTransiLeave.js";
import onLoadImg from "../utils/onLoadImg.js";

function ajax() {
	if ("scrollRestoration" in history) {
		history.scrollRestoration = "manual";
	}

	//window.canChangePage = true
	var pagesData = [];
	var pageTransiType = false;

	/**
	 * History stage changed
	 **/
	function historyStateChanged() {
		pageTransiType = false;
		var url = window.location.href.trimHttp();
		pageLoading(url, false);
	}

	/**
	 * link handler
	 **/
	function linkHandler(e) {
		// Get link href
		var link = $(this);
		if (link.is("[hreflang]") || !link[0].hasAttribute("href")) return;
		var href = link.attr("href");
		var url = href;

		// debug
		var protocol = location.protocol;
		var hostname = location.hostname;
		var port = location.port || "";
		var portdot = port.length ? ":" + port : "";

		url = url.replace("127.0.0.1", hostname + portdot);



		// if link is not a
		if ( link.hasClass("no-link") ){
			return;
		}
		if (
			href === undefined ||
			link[0].hasAttribute("target") ||
			href.indexOf('#') != -1
		) {
			return false;
		}

		// todo if starts with #
		//if (href.startsWith('#'))

		// todo if HREF is current page, do nothing
		if (document.URL === href) {
			e.preventDefault();
			return;
		}

		// rebuild URL if it's relative (please put full HREF, it's better, we kinda want to avoid to rebuild the HREF)
		if (!href.startsWith("http") && !href.startsWith("//localhost")) {
			var currentURL = location.href;
			var cleanURL = currentURL.substring(0, currentURL.lastIndexOf("/"));
			url = cleanURL + "/" + href;
		}

		// hide menu
		if (window.navDeployed) {
			window.navCanChange = true;
			$(".burger").trigger("click");
		}

		// trim href
		if (url !== undefined) {
			url = url.trimSlash().trimHttp();
		}

		// Check if it's an internal URL
		if (url.startsWith(location.hostname)) {

			e.preventDefault();
			if (!window.canChangePage) return;

			pageTransiType = link.attr("page-transi-type") || false;
			// Load page
			pageLoading(url, true);
		}
	}

	/**
	 * Save pages data for further use
	 **/
	function savePagesData(url, data) {
		url = url.replace(/\/$/, "").trimHttp();
		pagesData[url] = data;
	}

	/**
	 * Get pages data
	 **/
	function getPagesData(url) {
		url = url.replace(/\/$/, "").trimHttp();
		return pagesData[url];
	}

	/**
	 * page loading
	 **/
	function pageLoading(url, push) {
		pageTransiLeave();

		//return//debug

		// Check if data exists
		var data = getPagesData(url);

		if (data == undefined) {
			$.get(location.protocol + "//" + url, function(data) {
				// Save page data
				savePagesData(location.protocol + url, data); // console.log('success');

				pageCreate(data);
			}).fail(function() {
				// console.log('fail');
				// window.location = '/404';
				// console.log('redirect to : ' + location.protocol+url);
				window.location = location.protocol + "//" + url;
			});
		} else {
			pageCreate(data);
		}

		if (push) {
			history.pushState(null, null, location.protocol + "//" + url);
		}
	}

	/**
	 * Create new page
	 **/
	function pageCreate(data) {
		window.canReveal = false;

		// Convert data to HTML
		var html = $("<html/>");
		html.get(0).innerHTML = data;

		// Create new container
		var newContainer = html.find(".page-container");
		newContainer.insertBefore(".page-container:first");

		// hide new container (and prevent new one to be pushed)
		TweenMax.set(newContainer, {
			opacity: 0,
			position: "absolute",
			top: 0,
			left: 0,
			width: "100%",
			x: "-9000%"
		});

		var firstImgLoaded = false;
		onLoadImg(
			$(".global-container")
				.find("img")
				.eq(0),
			function() {
				firstImgLoaded = true;
			}
		);

		function updatePage() {
			if (!window.canChangePage) {
				requestAnimationFrame(updatePage);
				return;
			}

		/*	if (!firstImgLoaded) {
				requestAnimationFrame(updatePage);
				return;
			}*/
			let meta_q = `
			[name="description"],
			[name="keywords"],
			[name="twitter:card"],
			[name="twitter:title"],
			[name="twitter:image"],
			[name="keywords"],
			[property]
			`;
			// Switch metas
			$("head meta")
				.filter(meta_q)
				.remove();

			html
				.find("meta")
				.filter(meta_q)
				.insertAfter('head meta[name="viewport"]'); // Set title

			// $("head title").html(html.find("title").html());
			$("head title").replaceWith(html.find("head title"));
			$(".polylang--nav").replaceWith(html.find(".polylang--nav"));
	//		console.log(html.html());

			// Switch nav (able to display active UI)
			// $(".navbar--smallnav").html(html.find(".navbar--smallnav").html());
			// $('#header-nav').replaceWith(html.find('#header-nav'));
			// $('#header-nav-mobile').replaceWith(html.find('#header-nav-mobile'));
			$('.wp-foot').replaceWith(html.find('.wp-foot'));

			// Switch nav/foot
			// setTimeout(function(){
			//   // timeout prevent instant change of color
			//   $('.navbar--smallnav').replaceWith(html.find('.navbar--smallnav'));
			// }, 1000);

			// analytics
			if (typeof ga !== "undefined") {
				ga("set", {
					page: window.location.pathname,
					title: html.find("title").html()
				});
				ga("send", "pageview");
			}
			if (typeof gtag !== "undefined") {
				gtag("config", "UA-ID", { page_path: window.location.pathname });
			}

			// Switch
			TweenMax.set(newContainer, { clearProps: "all" });
			$(".page-container")
				.not(newContainer)
				.remove();

			initPage();
			pageTransiEnter();
		}

		updatePage();
	}

	/**
	 * events
	 **/
	var addEvents = function() {
		$("body").on("click", "a", linkHandler);
	};

	/**
	 * init
	 **/
	var init = function() {
		addEvents();
		$(window).bind("popstate", historyStateChanged);
	};
	init();
}

export default ajax;
