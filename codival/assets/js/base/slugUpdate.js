window.currentSlug = "";
window.oldSlug = "";
function slugUpdate() {
	window.oldSlug = window.currentSlug;
	window.currentSlug = $(".page-container").attr("slug");
	$("html").attr("page-slug", window.currentSlug);
}
export default slugUpdate;
