// base
import initSite from "./initSite.js"; // when site is init (not to be confused with when page is init)

import onLoadImg from "../utils/onLoadImg.js";
import normalizeRange from "../utils/normalizeRange.js";

import slugUpdate from "../base/slugUpdate.js";
import {siteLoaderExit, siteLoaderUpdate} from "../anim/siteLoader.js";

function preloader() {
	slugUpdate();

	//setTimeout(onLoad, 1000);

	function onLoad() {
		// loader values
		let $img = $("img");
		let countImg = $img.length || 0;
		let countImgLoaded = 0;

		// update
		function update() {
			siteLoaderUpdate(countImgLoaded, countImgLoaded);
			if (countImg === countImgLoaded) {
				setTimeout(allImgLoaded, 500);
			}
		}

		// load each img
		$img.each(function() {
			let $self = $(this);
			onLoadImg($self, function() {
				countImgLoaded++;
				update();
			});
		});

		// prevent bug
		if (countImg === 0) {
			countImg = 1;
			countImgLoaded = 1;
			update();
		}
	}

	/*
	 ** WHEN LOAD IS FINISEHD
	 */
	function allImgLoaded() {
		siteLoaderExit();
		window.canChangePage = true;
		initSite();
	}

	allImgLoaded()
}

export default preloader;
