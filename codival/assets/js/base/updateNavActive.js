function updateNavActive() {
	let currentURL = document.URL.trimSlash().trimHttp();
	// console.log("update");
	let $link = $(".sitenav--nav li a");

	$link.each(function() {
		let $self = $(this);
		if ($self.attr("href") === undefined) return;
		$self.parent().removeClass("current-link");
		let href = $self
			.attr("href")
			.trimSlash()
			.trimHttp();
		if (href === currentURL) $self.parent().addClass("current-link");
	});

	$(".menu-list>li").each(function() {
		$(this).removeClass("current-li");
		if($(this).find(".current-link").length){
			$(this).addClass("current-li");
		}
	});
}

export default updateNavActive;
