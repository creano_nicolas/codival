<svg viewBox="0 0 731.5 790" width="731.5" height="790">
  <path d="M462.5 786C715.7 786 921 580.7 921 327.5S715.7-131 462.5-131 4 74.3 4 327.5 209.3 786 462.5 786z" fill="none" stroke="#fe7c01" stroke-width="7" stroke-dasharray="4,28"/>
  <path d="M462.6 301.6v23.9h22.8v3.9h-22.8v24h-4.1v-24h-22.8v-3.9h22.8v-23.9h4.1z" fill="#fe7c01"/>
</svg>