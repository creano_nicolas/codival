<?php
  /*
  includesWithVar('svg/bullet-point', [
    "out" => "orange",
    "in"  => "green",
  ]);
  */
?>
<svg width="81" height="40" xmlns="http://www.w3.org/2000/svg">
  <g fill="none" fill-rule="evenodd">
    <g transform="translate(1 1)">
      <path d="M19 38c10.493 0 19-8.507 19-19S29.493 0 19 0 0 8.507 0 19s8.507 19 19 19z" class="stroke<?= $out; ?>" stroke-dasharray="8,11"/>
      <circle fill="none" class="fill<?= $in; ?>" cx="19" cy="19" r="4"/>
    </g>
  </g>
</svg>