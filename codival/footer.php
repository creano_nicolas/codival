<?php
wp_reset_postdata();
// Get les fields du theme option "Footer"
$lang = pll_current_language();

$footerField = [
	"address_title" => carbon_get_theme_option('footer_address_title_'.$lang),
	"address_desc" 	=> carbon_get_theme_option('footer_address_desc_'.$lang),

	"nums_title" 		=> carbon_get_theme_option('footer_nums_title_'.$lang),
	"nums_desc1" 		=> carbon_get_theme_option('footer_nums_desc1_'.$lang),
	"nums_desc2" 		=> carbon_get_theme_option('footer_nums_desc2_'.$lang),
	"nums_list" 		=> carbon_get_theme_option('footer_nums_list_'.$lang),

	"group_title" 	=> carbon_get_theme_option('footer_group_title_'.$lang),
	"group_list" 		=> carbon_get_theme_option('footer_group_list_'.$lang),
];
?>

		<!-- footer -->
		<footer>
			<div class="footgreen">
				<div class="footgreen--menu">
					<?php nav_footer(); ?>
				</div>
			</div>
			<div class="footwhite">
				<div class="footwhite--col">
					<div class="footwhite--title"><?= $footerField['address_title']; ?></div>
					<div class="footwhite--desc">
						<?= apply_filters('the_content', $footerField['address_desc']); ?>
					</div>
				</div>
				<div class="footwhite--col">
					<div class="footwhite--title"><?= $footerField['nums_title']; ?></div>
					<div class="footwhite--spec">
						<p class="footwhite--spec-label1"><?= $footerField['nums_desc1']; ?></p>
						<div class="footwhite--spec-selectcont">
							<select class="footwhite--spec-select">
								<?php foreach($footerField['nums_list'] as $nums) : ?>
									<option value="<?= $nums['number']; ?>"><?= $nums['text']; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<p class="footwhite--spec-label2"><?= $footerField['nums_desc2']; ?></p>
						<div class="footwhite--spec-result"></div>
					</div>
				</div>
				<div class="footwhite--col">
					<div class="footwhite--title"><?= $footerField['group_title']; ?></div>
					<ul class="footwhite--listlink">
						<?php foreach($footerField['group_list'] as $items) : ?>
							<li><a href="<?= $items['link']; ?>" class="no-link" target="_blank"><?= $items['title']; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="footwhite--col">
					<?php get_template_part('svg/codival-vertical-logo'); ?>
				</div>
			</div>
			<div class="footsub">
				<?php nav_footer_secondary(); ?>
			</div>
		</footer>
		<!-- /footer -->
	</div><!-- /global-container -->
</div><!-- scroll-container -->

<div class="wp-foot">
	<?php wp_footer(); ?>
	<?php edit_post_link('Edit page', '<p id="edit-post-link" style="position: fixed; top: 200px; right: 0; padding: 5px 10px; background: #000; color: #fff; z-index: 9999999">', '</p>', '' , 'no-link'); ?>
</div>

<script type="text/javascript">
  // Get template url in js
  var wpTemplateUrl = '<?= get_bloginfo("template_url"); ?>';
  var wpSiteUrl = '<?= get_site_url() ?>';
</script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/assets/js/lib/jquery-3.2.1.min.js"><\/script>')
</script>
<?php if(false): ?>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/dist/js/lib.min.js"></script>
	<script type="module" src="<?php echo get_template_directory_uri(); ?>/assets/js/app.js"></script>
<?php else: ?>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/dist/js/site.js"></script>
<?php endif; ?>
</body>
</html>
