<?php

namespace Carbon;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Create Carbon Fields Class.
 */
class Carbon {

	/**
	 * Datepicker options
	 */
	public $datepicker_options = array();


	/**
	 * Create CarbonField Field
	 *
	 * @param string $type (req) : Type of field
	 * @param string $slugbase (req) : container slug
	 * @param string $slug (req) : field slug
	 * @param string $desc (req) : field description
	 * @param bool $req : if field is required
	 * @param int $width : field width
	 */
	public static function create_field( $type, $slugbase, $slug, $desc, $req = false, $width = 100, $field_to_check = false, $field_value = false, $operator = '==' ) {

		$completed_slug = $slugbase . $slug;

		if( $field_to_check && $field_value ){

	        return Field::make( $type, $completed_slug, $desc )
	        	->set_width( $width )
	        	->set_required( $req )
				->set_conditional_logic( array(
						array(
							'field' => $field_to_check,
							'value' => $field_value,
						)
					) );

        } else {

	        return Field::make( $type, $completed_slug, $desc )
	        	->set_width( $width )
	        	->set_required( $req );

        }

	}


	/**
	 * Create CarbonField Field
	 *
	 * @param string $type (req) : Type of field
	 * @param string $slugbase (req) : container slug
	 * @param string $slug (req) : field slug
	 * @param string $desc (req) : field description
	 * @param string $association_type : post or 
	 * @param bool $req : if field is required
	 * @param int $width : field width
	 */
	public static function create_field_association( $slugbase, $slug, $desc, $association_type = 'post', $association_post_type = 'page', $width = 100, $min = 0, $max = 1000, $field_to_check = false, $field_value = false, $operator = '==' ) {

		$completed_slug = $slugbase . $slug;

        if( $field_to_check && $field_value ){

	        return Field::make( 'association', $completed_slug, $desc )
	        	->set_width( $width )
	        	->set_min( $min )
	        	->set_max( $max )
				->set_conditional_logic( array(
						array(
							'field' => $field_to_check,
							'value' => $field_value,
						)
					) )
	            ->set_types( array(
	                array(
	                    'type' => $association_type,
	                    'post_type' => $association_post_type,
	                ),
	            ) );

        } else {

	        return Field::make( 'association', $completed_slug, $desc )
	        	->set_width( $width )
	        	->set_min( $min )
	        	->set_max( $max )
	            ->set_types( array(
	                array(
	                    'type' => $association_type,
	                    'post_type' => $association_post_type,
	                ),
	            ) );

        }

	}	

}
