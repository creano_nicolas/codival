<?php


use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Create variables from Carbon Fields.
 */
class GetCarbon {

	/**
	 * Get CarbonField Field texts
	 *
	 * @param string $slugs (req) : array with the names of the text fields to transform to variable
	 * @param string $id : ID of the page (default to current page)
	 * @param string $complex : name of current loop value
	 */
	public static function get_fields( $slugs = array(), $id = false, $complex = false ) {

		if($complex){

			foreach ($slugs as $slug) {
					
				global ${ $slug };

				if(${ $complex }[ $slug ]){	

					${ $slug } = ${ $complex }[ $slug ];

				} else {

					${ $slug } = false;

				}

			}

		} else {

			if( $id ){

				foreach ($slugs as $slug) {
					
					global ${ $slug };

					if(carbon_get_post_meta( $id, $slug )){

						${ $slug } = carbon_get_post_meta( $id, $slug );

					} else {

						${ $slug } = false;

					}				

				}

			} else {

				foreach ($slugs as $slug) {
					
					global ${ $slug };	

					if(carbon_get_the_post_meta( $slug )){

						${ $slug } = carbon_get_the_post_meta( $slug );

					} else {

						${ $slug } = false;

					}		

				}

			}

		}

	}


	/**
	 * Get CarbonField Theme Option Field texts
	 *
	 * @param string $slugs (req) : array with the names of the text fields to transform to variable
	 * @param string $complex : name of current loop value
	 */
	public static function get_theme_fields( $slugs = array() ) {

		foreach ($slugs as $slug) {
			
			global ${ $slug };	

			if(carbon_get_theme_option( $slug )){

				${ $slug } = carbon_get_theme_option( $slug );

			} else {

				${ $slug } = false;

			}		

		}

	}	

	/**
	 * Return CarbonField Theme Option Field texts array
	 *
	 * @param string $slugs (req) : array with the names of the text fields to transform to variable
	 * @param string $complex : name of current loop value
	 */
	public static function get_theme_fields_array( $slugs = array() ) {

		$array_to_extract = array();

		foreach ($slugs as $slug) {

			if(carbon_get_theme_option( $slug )){

				${ $slug } = carbon_get_theme_option( $slug );

			} else {

				${ $slug } = false;

			}		

			$array_to_extract += array($slug => ${ $slug });

		}

		return $array_to_extract;

	}		


	/**
	 * Get CarbonField Field images
	 *
	 * @param string $slugs (req) : array with the names of the images fields to transform to variable
	 * @param string $id : ID of the page (default to current page)
	 * @param string $complex : name of current loop value
	 */
	public static function get_fields_image( $slugs = array(), $id = false, $complex = false, $size = '' ) {

		if($complex){

			foreach ($slugs as $slug) {

				global ${ $slug };
				
				if(${ $complex }[ $slug ]){
			
					global ${ $slug . '_alt' };	
					${ $slug } = wp_get_attachment_image_src(${ $complex }[ $slug ], $size)[0];
					${ $slug . '_alt' } = get_post_meta(${ $complex }[ $slug ], '_wp_attachment_image_alt', true);

				} else {

					${ $slug } = false;

				}			

			}

		} else {

			if( $id ){

				foreach ($slugs as $slug) {

					global ${ $slug };

					if(carbon_get_post_meta( $id, $slug )){
																	
						global ${ $slug . '_alt' };			
						${ $slug } = wp_get_attachment_image_src(carbon_get_post_meta( $id, $slug ), $size)[0];
						${ $slug . '_alt' } = get_post_meta(carbon_get_post_meta( $id, $slug ), '_wp_attachment_image_alt', true);

					} else {

						${ $slug } = false;

					}

				}	

			} else {

				foreach ($slugs as $slug) {

					global ${ $slug };
					
					if(carbon_get_the_post_meta( $slug )){
			
						global ${ $slug . '_alt' };	
						${ $slug } = wp_get_attachment_image_src(carbon_get_the_post_meta( $slug ), $size)[0];
						${ $slug . '_alt' } = get_post_meta(carbon_get_the_post_meta( $slug ), '_wp_attachment_image_alt', true);

					} else {

						${ $slug } = false;

					}

				}

			}

		}

	}


	/**
	 * Get CarbonField Field images
	 *
	 * @param string $slugs (req) : array with the names of the images fields to transform to variable
	 * @param string $id : ID of the page (default to current page)
	 * @param string $complex : name of current loop value
	 */
	public static function get_theme_fields_image( $slugs = array() ) {

		foreach ($slugs as $slug) {

			global ${ $slug };
			global ${ $slug . '_alt' };
			
			if(carbon_get_theme_option( $slug )){
						
				${ $slug } = wp_get_attachment_image_src(carbon_get_theme_option( $slug ), '')[0];
				${ $slug . '_alt' } = get_post_meta(carbon_get_theme_option( $slug ), '_wp_attachment_image_alt', true);

			} else {

				${ $slug } = false;

			}

		}

	}


	/**
	 * Rturn CarbonField Field images array
	 *
	 * @param string $slugs (req) : array with the names of the images fields to transform to variable
	 * @param string $id : ID of the page (default to current page)
	 * @param string $complex : name of current loop value
	 */
	public static function get_theme_fields_image_array( $slugs = array() ) {

		$array_to_extract = array();

		foreach ($slugs as $slug) {
			
			if(carbon_get_theme_option( $slug )){
						
				${ $slug } = wp_get_attachment_image_src(carbon_get_theme_option( $slug ), '')[0];
				${ $slug . '_alt' } = get_post_meta(carbon_get_theme_option( $slug ), '_wp_attachment_image_alt', true);

				$array_to_extract += array($slug => ${ $slug });
				$array_to_extract += array($slug . '_alt' => ${ $slug . '_alt' });

			} else {

				$array_to_extract += array($slug => false);

			}

		}

	}	



	/**
	 * Get CarbonField Field association
	 *
	 * @param string $slugs (req) : array with the names of the association fields to transform to variable
	 * @param string $id : ID of the page (default to current page)
	 * @param string $complex : name of current loop value
	 */
	public static function get_fields_association( $slugs = array(), $id = false, $complex = false ) {

		if($complex){

			foreach ($slugs as $slug) {
				
				global ${ $slug };

				if(${ $complex }[ $slug ]){

					global ${ $slug . '_id'};			
					${ $slug . '_id' } = ${ $complex }[ $slug ][0]['id'];
					${ $slug } = get_permalink( ${ $slug . '_id' } );	

				} else {

					${ $slug } = false;

				}	

			}

		} else {

			if( $id ){

				foreach ($slugs as $slug) {
								
					global ${ $slug };

					if(carbon_get_post_meta( $id, $slug )){

						global ${ $slug . '_id'};						
						${ $slug . '_id' } = carbon_get_post_meta( $id, $slug )[0]['id'];
						${ $slug } = get_permalink( ${ $slug . '_id' } );

					} else {

						${ $slug } = false;

					}

				}

			} else {

				foreach ($slugs as $slug) {
					
					global ${ $slug };

					if(carbon_get_the_post_meta( $slug )){

						global ${ $slug . '_id'};												
						${ $slug . '_id' } = carbon_get_the_post_meta( $slug )[0]['id'];
						${ $slug } = get_permalink( ${ $slug . '_id' } );	

					} else {

						${ $slug } = false;

					}		

				}

			}

		}

	}


	/**
	 * Return CarbonField Field association array
	 *
	 * @param string $slugs (req) : array with the names of the association fields to transform to variable
	 * @param string $id : ID of the page (default to current page)
	 * @param string $complex : name of current loop value
	 */
	public static function get_theme_fields_association_array( $slugs = array(), $id = false, $complex = false ) {

		$array_to_extract = array();

		foreach ($slugs as $slug) {

			if(carbon_get_theme_option( $slug )){
												
				${ $slug . '_id' } = carbon_get_theme_option( $slug )[0]['id'];
				${ $slug } = get_permalink( ${ $slug . '_id' } );	

				$array_to_extract += array( $slug => ${ $slug });
				$array_to_extract += array( $slug . '_id' => ${ $slug . '_id' });

			} else {

				$array_to_extract += array( $slug => false );

			}		

		}

		return $array_to_extract;

	}			

}
