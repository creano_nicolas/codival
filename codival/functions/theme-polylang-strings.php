<?php
/*------------------------------------*\
		POLYlANG - register strings
\*------------------------------------*/
pll_register_string("filtrer_par", "Filtrer par");
pll_register_string("voir_aussi", "Voir aussi");
pll_register_string("en_savoir_plus", "En savoir plus");
pll_register_string("search", "Rechercher...");
pll_register_string("no_result", "Désolé, rien n'a été trouvé.");
pll_register_string("fd_home", "File d_ariane");

pll_register_string("title_actu", "Actualités", "Actualités");
pll_register_string("title_actu_all", "Toutes les actualités", "Actualités");
pll_register_string("title_actu_url", "http://127.0.0.1/codival/actualites/", "Actualités");
