<?php
/*------------------------------------*\
		Carbon Fields
\*------------------------------------*/
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
	include_once(get_template_directory() . '/carbon/carbon.php');
}

?>
