<?php // MOBILE MENU ?>
<div id="fwmb-menu" class="fwmb-menu-wrapper">
	<div class="fwmb-menu-head">
		<ul class="menu-switchlang">
			<?php pll_the_languages(array('display_names_as' =>'slug', 'hide_curent' => 0, 'hide_if_empty' => 0, 'hide_if_no_translation' => 0)); ?>
		</ul>
	</div>
	<?php $menu->display_menu_mobile(); ?>
	<form class="sitenav-searchbar sitenav-searchbar--mobile" method="GET" action="<?= $home_url; ?>" role="search">
		<div class="sitenav-searchbar--inner">
			<!-- <input type="text"> -->
			<input class="search-input" type="search" name="s" placeholder="<?php pll_e("Rechercher...") ?>">
			<button class="btn btn-blue">OK</button>
		</div>
	</form>

</div>
