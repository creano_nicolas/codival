<?php
    function getModuleByName($modules, $name) {
        if(!is_null($modules)) {
            foreach($modules as $module) {
                if($module["_type"] === $name) {
                    return $module;
                }
            }
        }

        return false;
		}


	$searchresults = array();
	if(isset($_GET['s']) && !empty($_GET['s'])){

    $myposts = [];
    $request = $_GET['s'];


    // Get all posts with the post_type 'membres' and 'projets'
		$args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'orderby'          => 'post_type',
		'order'            => 'ASC',
		'post_type'        => 'post',
    );


    // Get all posts with the post_type 'page'
    $args2 = array(
        'posts_per_page' => -1,
        'offset' => 0,
        'orderby' => 'post_type',
        'order' => 'ASC',
        'post_type' => 'page'
    );

    /*
     * La requête pour un post_type 'any' ou un array('page', 'membres', 'projets)
     * ne fonctionne étonnamant pas à cause de 'page'. D'où 2 requêtes séparer.
     */
    $myposts[] = get_posts( $args );
		$myposts[] = get_posts( $args2 );

		function ContainRequest($value, $needle){
			//lowercase , replace "-" with space, clean html
			$v = str_replace("-" ," ", strtolower (htmlentities($value) ));
			$q = str_replace("-" ," ", strtolower (htmlentities($needle) ));
			return strpos($v, $q) !== false;
		}

		function WysiwygContainRequest($type_module, $needle)
		{
			$searchingFields = [
				"markup" => [
					"text"
				]
			];

			foreach($type_module as $module) {
				foreach($searchingFields as $key => $value) {
					if($module["_type"] == $key) {
						foreach($value as $subValue){
							if(ContainRequest($module["text"], $needle)) {
								return true;
							}
						}
					}
				}
			}

			return false;
		}

    foreach ($myposts as $mypost) {
        foreach($mypost as $value) {
						$type_module = carbon_get_post_meta($value->ID, 'type_de_module');

            // echo "<pre>";
            // var_dump($value->post_title);
						// var_dump(strpos($value->post_name, $request));
						// var_dump(carbon_get_post_meta($value->ID, 'type_de_module'));
						// echo "</pre>";

            if(
							ContainRequest($value->post_title, $request) ||
							ContainRequest($value->post_content, $request) ||
							WysiwygContainRequest($type_module, $request)
            ) {
				$desc = "";
                // Si le titre n'est pas vide
                if ( $value->post_title != "" ){
                    // Si le post est de type 'page'
                    if($value->post_type == 'page') {
											foreach ($type_module as $module) {
												switch ($module['_type']) {
													case 'markup':
														$desc .= $module['text']." ";
														break;
													case 'twoblocks':
														$desc .= $module['text_1']." ".$module['text_2']." ";
														break;
												}
											}
                    }
                    // Si le post est de type 'membres'
                    else if ($value->post_type == 'membres') {
												$desc = carbon_get_post_meta($value->ID, 'membre_text');
                        if($desc != ""){
														$desc = strip_tags($desc);
                        }
                        //echo "<a class='btn' href='" . get_the_permalink($value->ID) . "'>En savoir plus<i class='btn--arrow'></i></a>";
                    }
                    // Si le post est de type 'projets'
                    else if ($value->post_type == 'projets'){
                        $desc = carbon_get_post_meta($value->ID, 'projet_text_1') ." ". carbon_get_post_meta($value->ID, 'projet_text_2');
                        if($desc != ""){
                            $desc = strip_tags($desc);
                        }
                       // echo "<a class='btn' href='" . get_the_permalink($value->ID) . "'>En savoir plus<i class='btn--arrow'></i></a>";
										}
										$maxdesc_len = 300;
										$searchresults[] = array(
											"title" => $value->post_title,
											"desc" => strlen($desc) > $maxdesc_len ? substr($desc, 0, $maxdesc_len) . '...' : $desc,
											"link" => get_the_permalink($value->ID)
										);
                }
            }

        }
		}
	}
?>
<div class="search-result-wrapper">
<?php if(count($searchresults) > 0) : ?>
	<?php foreach($searchresults as $result): ?>
		<a href="<?= $result["link"] ?>">
			<div class="search-result">
				<h2><?= $result["title"] ?></h2>
				<?php if(!empty($result["desc"])): ?>
					<p><?= $result["desc"]; ?></p>
				<?php endif; ?>
				<a href="<?= $result["link"] ?>" class="arcta arcta__arrow">
					<span class="arcta--text"><?= pll_e("En savoir plus") ?></span>
					<span class="arcta--arrow"><?php get_template_part('svg/button-arrow'); ?></span>
				</a>
			</div>
		</a>
	<?php endforeach; ?>
<?php else: ?>
	<p class="search-noresult"><?= pll_e("Désolé, rien n'a été trouvé.") ?></p>
<?php endif; ?>
</div>
