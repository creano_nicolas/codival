<?php

// Module Statique
$modules = carbon_get_the_post_meta('type_de_module');
foreach ($modules as $module) {
	switch ($module['_type']) {
		case 'markup':
			include(locate_template('modules/module-markup.php'));
			break;
		case 'images':
			include(locate_template('modules/module-images.php'));
			break;
		case 'expertises':
			include(locate_template('modules/module-expertises.php'));
			break;
		/*case 'videoquote':
			include(locate_template('modules/module-videowithquote.php'));
			break;
		case 'multiblocks':
			include(locate_template('modules/module-multiblocks.php'));
			break;
		case 'logocaroussel':
			include(locate_template('modules/module-logocaroussel.php'));
			break;
		case 'members':
			include(locate_template('modules/module-members.php'));
			break;
		case 'faq':
			include(locate_template('modules/module-faq.php'));
			break;
		case 'reseau':
			include(locate_template('modules/module-reseau.php'));
			break;*/

		// codival
		case 'bhead':
			include(locate_template('modules/module-bhead.php'));
			break;
		case 'codibest':
			include(locate_template('modules/module-codibest.php'));
			break;
		case 'acti':
			include(locate_template('modules/module-acti.php'));
			break;
		case 'codires':
			include(locate_template('modules/module-codires.php'));
			break;
		case 'logotabs':
			include(locate_template('modules/module-logotabs.php'));
			break;
		case 'chiffres':
				include(locate_template('modules/module-chiffres.php'));
				break;
		case 'map':
				include(locate_template('modules/module-map.php'));
				break;
	}
}
