<?php get_header(); ?>

<div class="page-container" data-slug="search"><?php /* data-slug="home" ===> Ajoute la class "slug--Home" */ ?>
    <section class="page-content">
        <div class="content-container content-container__sm">
					<?php get_template_part('templates/loop_carbon'); ?>
        </div>
    </section>
</div>

<?php get_footer(); ?>
