<?php

get_header();

$contentPost = [
	"photo" => (carbon_get_post_meta( get_the_ID(), "membre_photo") != "" ? carbon_get_post_meta( get_the_ID(), "membre_photo") : null),
	"job" => carbon_get_post_meta( get_the_ID(), "membre_job"), // get url path of the image
	"description" => (carbon_get_post_meta( get_the_ID(), "membre_text") != "" ? carbon_get_post_meta( get_the_ID(), "membre_text") : "Aucune biographie pour ce membre. Veuillez l'éditer dans le back-office"),
	"management" => carbon_get_post_meta( get_the_ID(), "membre_management")
];
?>

<div class="page-container" data-slug="project-detail">
    <section class="page-content">
			<div class="content-container content-container__sm">
				<div class="breadcrumb">
					<?php get_breadcrumb(); ?>
				</div>
			</div>
			
			<ul class="membersbook">
				<li class="membersbook--member membersbook--member--big <?php if($contentPost['management']) echo "membersbook--member--management" ?> scroll-reveal" reveal-offset="300">
						<img class="membersbook--member--photo" src="<?= $contentPost['photo']; ?>" alt="<?= the_title(); ?>">
						<h3 class="membersbook--member--name"><?php the_title(); ?></h3>
						<p class="membersbook--member--job"><?= $contentPost['job']; ?></p>
				</li>
			</ul>
			<div class="content-container content-container__sm">
				<div class="markup mbl"><?= $contentPost["description"]; ?></div>
			</div>
    </section>
</div>

<?php get_footer(); ?>
