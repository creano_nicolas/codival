<?php get_header(); ?>
<div class="page-container" data-slug="all">
	<section class="page-content">
		<header class="page-content--header content-container content-container__sm">
			<div class="breadcrumb">
				<?php get_breadcrumb(); ?>
			</div>
			<div class="sqtitle">
				<div class="sqtitle--in">
					<div class="decoangle decoangle__orange decoangle__tr decoangle__sm"></div>
					<div class="decoangle decoangle__orange decoangle__bl decoangle__md"></div>
					<h1 class="sqtitle--title"><?php the_title(); ?></h1>
				</div>
			</div>
		</header>
		<?php
			$module["photos"] = carbon_get_the_post_meta("header_photos");
			include(locate_template('modules/module-images.php'));
		?>
		<?php include(locate_template('templates/modules-libres.php')); ?>
		<?php //include(locate_template('modules/module-similar.php')); ?>
	</section>
</div>
<?php get_footer(); ?>
