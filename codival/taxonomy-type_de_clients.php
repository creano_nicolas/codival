<?php /* Template Name: Template - produits & services */
get_header();
$current_cat_slug = get_queried_object()->slug;
$current_cat_name = get_queried_object()->name;
$lang = pll_current_language();
$title = carbon_get_theme_option('th_services_title_'.$lang);
$all = carbon_get_theme_option('th_services_all_'.$lang);
?>


<div class="page-container" data-slug="contact">
	<section class="page-content page-content--grey">
    <div class="content-container">
			<div class="breadcrumb">
				<?php get_breadcrumb(); ?>
			</div>
		</div>
		<?php //get_template_part('templates/loop'); ?>
		<header class="page-content--header content-container">
			<h1 class="title-page"><?= $title ?></h1>
		</header>
		<section class="content-container">
			<div class="gallery-filters">
				<span><?= pll_e("Filtrer par") ?> :</span>
				<ul>
					<li><a href="<?= get_post_type_archive_link('services'); ?>"><?= $all ?></a></li>
					<?php
					$terms = get_terms( array(
						'taxonomy' => 'type_de_clients',
						'orderby'  => 'name',
						'order'  => 'ASC',
						'hide_empty' => 1
					) );

					foreach ($terms as $term):
						$activ_class = ($current_cat_slug == $term->slug) ? 'is-active' : '';
					?>
						<li class="<?= $activ_class ?>"><a href="<?= get_term_link( $term ); ?>" data-letters="<?= $term->name; ?>"><?= $term->name; ?></a></li>
					<?php endforeach; ?>
				</ul>
				</div>
		</section>
		<section class="module">
				<div class="content-container">
				<h2 class="title-section title-section--<?= $current_cat_slug ?>"><?= $current_cat_name ?></h2>
				<div class="card-container mbl">
				<?php
					$loop = new WP_Query( array(
						'post_type' => 'services',
						'orderby' => 'date',
						'order' => 'ASC',
						'tax_query' => array(
							array(
								'taxonomy' => 'type_de_clients',
								'field'    => 'slug',
								'terms'    => $current_cat_slug,
							),
						),
					) );
					while ( $loop->have_posts() ) : $loop->the_post();
					$name = get_the_title();
					$current_id = get_the_ID();
					$service_terms = get_the_terms($current_id, "type_de_services");
					$description = carbon_get_post_meta( get_the_ID(), "servicepost_description");
				?>
					<a href="<?= get_the_permalink(); ?>" class="card card--<?= $current_cat_slug ?>">
						<?php foreach($service_terms as $sterm): ?>
							<span><?= $sterm->name ?></span>
						<?php endforeach; ?>
						<h4><?= $name ?></h4>
						<p><?= $description ?></p>
					</a>
				<?php endwhile; ?>
					</div>
			</div>
		</section>
	</section>
</div>

<?php get_footer(); ?>
