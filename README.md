# THEME Codival #


## Attention ##

Ce repo est un kickstarter et doit rester un kickstarter.

Pour chaque nouveau projet, cloner ce repo et gérer votre projet.


## Description ##

Ce thème est à mettre à la racine du dossier Theme, soit monsite.com/wp-content/theme/

Donc dans monsite.com/wp-content/theme/ je devrais avoir **gulpfile.js**, **package.json**, le dossier **/kickstarter-theme** etc.


## Gulp

### Serveur local

BrowserSync (avec Gulp) premet de simuler un serveur local, afin de réinjecter le CSS et reload à chaque update du PHP/JS.

BrowserSync lance un serveur local (*localhost:3000*) à partir d'un proxy de *http://127.0.0.1/monsite/*.

**1 - configurer le proxy dans le *gulpfile.js* ** (exemple : *http://127.0.0.1/monsite/*)

**2 - ALTERNATIF - Si le site doit être mis sur une preprod en ligne, configurer les url dans le fichier JS** (*kickstarter-theme/assets/js/main.js*, tout en bas du fichier (**var hostPreprod**))


### installation

Utiliser la ligne de commande, au niveau du *gulpfile.js* (là ou se trouve les thèmes) :


```
$ npm install
```
or
```
$ yarn
```


### use

```
$ gulp
```
